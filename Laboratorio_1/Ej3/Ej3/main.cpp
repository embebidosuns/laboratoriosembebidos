/*
 * Ej3.cpp
 *
 * Created: 19/08/2018 18:04:47
 * Author : Mayu
 */ 

#include <avr/io.h>
#include <util/delay.h>

bool on = false;

void toggleLed(){
	PORTB ^= (1<<PB5);
	on = !on;
}

int status = 0;
int cantStatus = 6;

void changeStatus(){
	status = (status+1)%cantStatus;
}

int buttonState;       // the current reading from the input pin
int lastButtonState = 0;   // the previous reading from the input pin
const int debounceDelay = 50;
static int delay = 0;

int main(void)
{
	DDRB |= (1<<DDB5);
    /* Replace with your application code */
	DDRD &= ~(1<<DDD2);
	
	//apago el led
	PORTB &= ~(1<<PB5);

    while (1)
    {
	    //Leer el estado del pulsador
	    buttonState = PIND & (1<<PD2);

	    //Si cambi? el estado del pulsador...
	    if (buttonState != lastButtonState)
	    {
		    //Se espera un tiempo para evitar leer el ruido del rebote del pulsador
		    _delay_ms(50);
			delay += 50;
		    //Se vuelve a leer el estado del pulsador
		    buttonState = PIND & (1<<PD2);

		    //Si el cambio se mantiene, se interpreta como evento de keydown/keyup v?lido
		    if (buttonState != lastButtonState)
		    {
			    //El pulso es v?lido, se implementa la l?gica del sistema

			    //Si no est? presionado el pulsador (se detect? un keyup)...
			    if (buttonState)
			    {
				    changeStatus();//Se modifica el estado del led (toggle v?a XOR)
			    }

			    //Si hubo cambios v?lidos actualizo el valor de lectura anterior
			    lastButtonState = buttonState;
		    }
	    }
		_delay_ms(50);
		delay+=50;
		if(status==0){
			//ENCENDIDO
			if(!on){
				toggleLed();
			}
		}
		if(status==1 && delay >= 1000){
			//0.5 HZ 1 segundo prendido 1 segundo apagado
			toggleLed();
			delay = 0;
		}
		if(status==2 && delay >= 500){
			//1 Hz 0.5 seg prendido, 0.5 seg apagado
			toggleLed();			
			delay = 0;

		}
		if(status==3 && delay >= 250){
			//2 Hz 0.25 seg prendido 0.25 seg apagado
			toggleLed();
			delay = 0;
		}
		if(status==4){
			//0.5 seg encendido, 1 seg apagado
			
			if(!on){
				//apago 1 seg
				if(delay >= 1000){
					toggleLed();
					delay = 0;
				}
				
			}
			else{
				//enciendo 0.5 seg
				if(delay >= 500){
					toggleLed();
					delay = 0;
				}
			}
		}
		if(status==5){
			//APAGADO
			if(on){
				toggleLed();
			}
		}
		
    }
}
