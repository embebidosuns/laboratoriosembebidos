/*
 * ej4_6.cpp
 *
 * Created: 21/08/2018 20:34:20
 * Author : Mayu
 */ 

#include <avr/io.h>
#include <util/delay.h>
#define	USART_H


void iniciar_serial(){
	UCSR0B=0b00011000;	//transmisi�n y recepci�n habilitados a 8 bits
	UCSR0C=0b00000110;	//as�ncrono, sin bit de paridad, 1 bit de parada a 8 bits
	UBRR0=103;			//para una velocidad de 9600 baudios con un oscilador de 16Mhz
}

unsigned char recibe_caracter(){
	if(UCSR0A&(1<<7)){  //si el bit7 del registro UCSR0A se ha puesto a 1
		return UDR0;    //devuelve el dato almacenado en el registro UDR0
	}
	else
		return 0 ;
}

void envia_caracter(unsigned char caracter){
	while(!(UCSR0A&(1<<5)));    // mientras el registro UDR0 est� lleno espera
	UDR0 = caracter;            //cuando el el registro UDR0 est� vacio se envia el caracter
}

void envia_cadena_usart(char* cadena){	 //cadena de caracteres de tipo char
	while(*cadena !=0x00){				//mientras el �ltimo valor de la cadena sea diferente
		//a el caracter nulo
		envia_caracter(*cadena);	//transmite los caracteres de cadena
		cadena++;						//incrementa la ubicaci�n de los caracteres en cadena
		//para enviar el siguiente caracter de cadena
	}
}
bool on = false;

void toggleLed(){
	PORTB ^= (1<<PB5);
	on = !on;
}

bool es_valido(char c){
	return	c=='1'
			|| c=='2'
			|| c=='3'
			|| c=='4'
			|| c=='5'
			|| c=='6';
}

int main(void)
{
	iniciar_serial();
	char c, c_ant = '1';
	
	DDRB |= (1<<DDB5);
	//apago el led
	PORTB &= ~(1<<PB5);
    /* Replace with your application code */
    while (1) 
    {
		c = recibe_caracter();
		
		if(c>0 && es_valido(c))
		{
			envia_caracter(c);
			c_ant = c;
		}
		
		
		if(c_ant=='1'){
			//ENCENDIDO
			if(!on){
				toggleLed();
			}
		}
		if(c_ant=='2'){
			//0.5 HZ 1 segundo prendido 1 segundo apagado
			toggleLed();
			_delay_ms(1000);
		}
		if(c_ant=='3'){
			//1 Hz 0.5 seg prendido, 0.5 seg apagado
			toggleLed();
			_delay_ms(500);
		}
		if(c_ant=='4'){
			//2 Hz 0.25 seg prendido 0.25 seg apagado
			toggleLed();
			_delay_ms(250);
		}
		if(c_ant=='5'){
			//0.5 seg encendido, 1 seg apagado
			
			if(on){
				//apago 1 seg
				toggleLed();
				_delay_ms(1000);
			}
			else{
				//enciendo 0.5 seg
				toggleLed();
				_delay_ms(500);
			}
		}
		if(c_ant=='6'){
			//APAGADO
			if(on){
				toggleLed();
			}
		}
		/**/
    }
}

