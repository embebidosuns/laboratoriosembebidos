#include "funciones.h"
#include "Arduino.h"


int8_t parte_entera(double input){
	return (int) input;
}

int8_t parte_decimal(double input){
	int entero = (int)input;
	return (int) ((input - entero) * 100.0f);
}



void convert_string(int8_t entera,int8_t decimal, char** toRet)
{
	char* ret = *toRet;
	ret[6] = 0;
	
	ret[0] = (entera/100) % 10 + 48;
	ret[1] = (entera/10) % 10 + 48;
	ret[2] = entera % 10 + 48 ;
	ret[3] = '.';
	ret[4] = (decimal/10) %10 + 48;
	ret[5] = decimal %10 + 48;
	
}

