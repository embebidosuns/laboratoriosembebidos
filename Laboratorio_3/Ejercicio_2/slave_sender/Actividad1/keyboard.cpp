#include "keyboard.h"
/*
char msgs[5][17] = {
	" Right Key:  OK ",
	" Up Key:     OK ",
	" Down Key:   OK ",
	" Left Key:   OK ",
	" Select Key: OK "};
*/
int adc_key_val[5] = { 120, 250, 450, 650, 800 };//CALCULAR!!!!
int key_val_to_tecla[5] = { TECLA_RIGHT, TECLA_UP, TECLA_DOWN, TECLA_LEFT, -1};
int prev_lcd_key;
int new_lcd_key;
int NUM_KEYS = 5;
void (*onKeyUp[9])(void);
void (*onKeyDown[9])(void);

// Botones Externos
volatile uint8_t portbhistory = 0xFF;     // default is high because the pull-up
volatile bool key_up_array[9] = {false,false,false,false,false,false,false,false,false};
volatile bool key_down_array[9] = {false,false,false,false,false,false,false,false,false};

ISR (PCINT1_vect)
{
	//hacer el debounce
	uint8_t changedbits;

	changedbits = PINC ^ portbhistory;
	portbhistory = PINC;
	if(changedbits & (1 << PINC2))
	{
		//delay(50);//debounce
		//changedbits = PINC ^ portbhistory;
		//if(changedbits & (1 << PINC2)){
			//portbhistory = PINC;
		if(PINC & (1 << PINC2)){
			//flanco ascendente
			key_down_array[BOTON_A2] = true;
			key_up_array[BOTON_A2] = false;
		}
		else{
			//flanco descendente
			key_up_array[BOTON_A2] = true;
			key_down_array[BOTON_A2] = false;
		}
	//}
	}

	if(changedbits & (1 << PINC3))
	{
		
		
			//PCINT3 changed
		if(PINC & (1 << PINC3)){
			//flanco ascendente
			key_down_array[BOTON_A3] = true;
			key_up_array[BOTON_A3] = false;
		}
		else{
			//flanco descendente
			key_up_array[BOTON_A3] = true;
			key_down_array[BOTON_A3] = false;
		}
		
	}

	if(changedbits & (1 << PINC4))
	{
		if(PINC & (1 << PINC4)){
			//flanco ascendente
			key_down_array[BOTON_A4] = true;
			key_up_array[BOTON_A4] = false;
		}
		else{
			//flanco descendente
			key_up_array[BOTON_A4] = true;
			key_down_array[BOTON_A4] = false;
		}
		
	}

	if(changedbits & (1 << PINC5))
	{
	
		if(PINC & (1 << PINC5)){
			//flanco ascendente
			key_down_array[BOTON_A5] = true;
			key_up_array[BOTON_A5] = false;

		}
		else{
			//flanco descendente
			key_up_array[BOTON_A5] = true;
			key_down_array[BOTON_A5] = false;
		}
		
	}
	
	
	
}

int lastInput;

// Convert ADC value to key number
int get_key()
{
	
	int k;
	for (k = 0; k < NUM_KEYS; k++)
		if (lastInput < adc_key_val[k]) {
			//prev_lcd_key = new_lcd_key;
			return key_val_to_tecla[k];
			
		}

	if (k >= NUM_KEYS)
		k = -1;     // No valid key pressed

	return k;
	/**/
}




void keyboard_callback(int input){
	lastInput = input;

}
struct adc_cfg cfg = {0, keyboard_callback};

void keyboard_init() {
	prev_lcd_key = -1;
	new_lcd_key = -1;
	adc_init(&cfg);
	
	DDRC &= ~(1 << DDC2);     // Clear the PC2 pin
	DDRC &= ~(1 << DDC3);     // Clear the PC3 pin
	DDRC &= ~(1 << DDC4);     // Clear the PC4 pin
	DDRC &= ~(1 << DDC5);     // Clear the PC5 pin

	PORTC |= (1 << PORTC2) | (1 << PORTC3) | (1 << PORTC4) | (1 << PORTC5);    // turn On the Pull-up
	// PC2 is now an input with pull-up enabled

	PCICR |= (1 << PCIE1);    // set PCIE1 to enable PCMSK1 scan
	PCMSK1 |= (1 << PCINT10) | (1 << PCINT11) | (1 << PCINT12) | (1 << PCINT13) ;  // set PCINT10 to trigger an interrupt on state change
}

void keyboard_loop() {
	adc_loop();
	
	new_lcd_key = get_key();
	int debounce_aux = new_lcd_key;
	if (prev_lcd_key != new_lcd_key)
	{
		delay(100);
		new_lcd_key = get_key();
		if (new_lcd_key == debounce_aux)
		{
			if (new_lcd_key != -1)
			//onKeyDown[new_lcd_key]();
			key_down_array[new_lcd_key] = true;
			else
			key_up_array[prev_lcd_key] = true;
			//onKeyUp[prev_lcd_key]();
			prev_lcd_key = new_lcd_key;
		}
	}/**/
	
	//aux_callback(lastInput);
	// LCD Key Pressed
	if(key_down_array[TECLA_UP]){
		delay(50);
		if(key_down_array[TECLA_UP]){
			key_down_array[TECLA_UP] = false;
			onKeyDown[TECLA_UP]();
		}
	}
	if(key_up_array[TECLA_UP]){
		delay(50);
		if(key_up_array[TECLA_UP]){
			key_up_array[TECLA_UP] = false;
			onKeyUp[TECLA_UP]();
		}
	}
	
	if(key_down_array[TECLA_DOWN]){
		delay(50);
		if(key_down_array[TECLA_DOWN]){
			key_down_array[TECLA_DOWN] = false;
			onKeyDown[TECLA_DOWN]();
		}
	}
	if(key_up_array[TECLA_DOWN]){
		delay(50);
		if(key_up_array[TECLA_DOWN]){
			key_up_array[TECLA_DOWN] = false;
			onKeyUp[TECLA_DOWN]();
		}
	}
	
	if(key_down_array[TECLA_LEFT]){
		delay(50);
		if(key_down_array[TECLA_LEFT]){
			key_down_array[TECLA_LEFT] = false;
			onKeyDown[TECLA_LEFT]();
		}
	}
	if(key_up_array[TECLA_LEFT]){
		delay(50);
		if(key_up_array[TECLA_LEFT]){
			key_up_array[TECLA_LEFT] = false;
			onKeyUp[TECLA_LEFT]();
		}
	}
	
	if(key_down_array[TECLA_RIGHT]){
		delay(50);
		if(key_down_array[TECLA_RIGHT]){
			key_down_array[TECLA_RIGHT] = false;
			onKeyDown[TECLA_RIGHT]();
		}
	}
	if(key_up_array[TECLA_RIGHT]){
		delay(50);
		if(key_up_array[TECLA_RIGHT]){
			key_up_array[TECLA_RIGHT] = false;
			onKeyUp[TECLA_RIGHT]();
		}
	}
	
	//botones externos
	if(key_down_array[BOTON_A2]){
		delay(50);
		if(key_down_array[BOTON_A2]){
			key_down_array[BOTON_A2] = false;
			onKeyDown[BOTON_A2]();
		}
	}
	if(key_up_array[BOTON_A2]){
		delay(50);
		if(key_up_array[BOTON_A2]){
			key_up_array[BOTON_A2] = false;
			onKeyUp[BOTON_A2]();
		}
	}
	if(key_down_array[BOTON_A3]){
		delay(50);
		if(key_down_array[BOTON_A3]){
			key_down_array[BOTON_A3] = false;
			onKeyDown[BOTON_A3]();
		}
	}
	if(key_up_array[BOTON_A3]){
		delay(50);
		if(key_up_array[BOTON_A3]){
			key_up_array[BOTON_A3] = false;
			onKeyUp[BOTON_A3]();
		}
	}
	if(key_down_array[BOTON_A4]){
		delay(50);
		if(key_down_array[BOTON_A4]){
			key_down_array[BOTON_A4] = false;
			onKeyDown[BOTON_A4]();
		}
	}
	if(key_up_array[BOTON_A4]){
		delay(50);
		if(key_up_array[BOTON_A4]){
			key_up_array[BOTON_A4] = false;
			onKeyUp[BOTON_A4]();
		}
	}
	if(key_down_array[BOTON_A5]){
		delay(50);
		if(key_down_array[BOTON_A5]){
			key_down_array[BOTON_A5] = false;
			onKeyDown[BOTON_A5]();
		}
	}
	if(key_up_array[BOTON_A5]){
		delay(50);
		if(key_up_array[BOTON_A5]){
			key_up_array[BOTON_A5] = false;
			onKeyUp[BOTON_A5]();
		}
	}	

}

void key_down_callback(void (*handler)(), int tecla) {
	onKeyDown[tecla] = handler;
}

void key_up_callback(void (*handler)(), int tecla) {
	onKeyUp[tecla] = handler;
}