/*
 * mensajes.h
 *
 * Created: 09/10/2018 15:11:47
 *  Author: Mayu
 */ 


#ifndef MENSAJES_H_
#define MENSAJES_H_

#define OBTENER_LUX		'A'
#define OBTENER_MAX		'B'
#define OBTENER_MIN		'C'
#define OBTENER_PROM	'D'
#define OBTENER_TODO	'E'
#define RESPONDER_LUX	'F'
#define RESPONDER_MAX	'G'
#define RESPONDER_MIN	'H'
#define RESPONDER_PROM	'I'
#define RESPONDER_TODO	'J'

#define INIT_CHAR		'$'
#define END_CHAR		'#'
#define ESC_CHAR		 0
#define ERROR_CHAR		'X'

#define TECLA_UP		'K'
#define TECLA_DOWN		'L'
#define TECLA_LEFT		'M'
#define TECLA_RIGHT		'N'
#define BOTON_A2		'0'

#endif /* MENSAJES_H_ */
