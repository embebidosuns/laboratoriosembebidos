#include "mraa.hpp"

#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>

#include <iostream>
#include "mensajes.h"
using namespace std;

mraa::Gpio* d_pin = NULL;

mraa::I2c* i2c;

uint8_t rx_tx_buf[16];
// Maximo 6 datos de 2 bytes c/u
//uint8_t payload[2 * 6];
uint8_t payload_size = 0;
uint8_t msg_type;
uint8_t last_byte;
char recv_buffer[256];
int staph = 0;


void clear_buffer() {
	for(int j = 0; j < 16; j++) {
		rx_tx_buf[j] = 0;
	}
}

uint8_t read_byte() {
	uint8_t buffer[1];
	i2c->read(buffer, 1);
	return buffer[0];
}

int read_payload(uint8_t size, float* data, int newsockfd) {
	clear_buffer();
	uint8_t buffer[size * 2];
	i2c->read(buffer, size * 2);

	/* Write a response to the client */
   int n = (staph == 0) ? write(newsockfd, buffer, size * 2) : -1;

   int entera;
   int decimal;
   for(int j = 0; j < size; j++) {
       entera = (int) buffer[j * 2] - 1;//read_byte();
       decimal = (int) buffer[j * 2 + 1] - 1;//read_byte();
       data[j] = entera + decimal * .01f;
   }
   if (n < 0) {
	  cout << "Connection lost" << "\n";
	  close(newsockfd);
	   //perror("ERROR writing to socket");
	  //exit(1);
	  return 1;
   }
   return 0;
}

void send_request(char type) {
	uint8_t* data = (uint8_t*) malloc(sizeof(uint8_t) * 4);
	data[0] = '$';
	data[1] = 0;
	data[2] = type;
	data[3] = '#';
	i2c->write(data, 4);
	//cout << "[Sent Packet] Type: " << data[2] << " Size: " << (int) data[1] << "\n";
	free(data);
}

int receive_response(float** buf, int newsockfd) {
	clear_buffer();
	//cout << "Awaiting Response... \n";
	// Espero a leer un $
	while(rx_tx_buf[0] != '$') {
		//cout << "Awaiting Response... \n";
		i2c->read(rx_tx_buf, 1);
	}
	//cout << "Got Response... \n";

	// Leer el Tama�o del Payload
	payload_size = read_byte();

	// Leo el Payload en funcion del tama�o
	*buf = (float*) malloc(sizeof(float) * payload_size);
	int ret = read_payload(payload_size, *buf, newsockfd);

	// Leo el tipo
	msg_type = read_byte();

	// Leo el fin
	last_byte = read_byte();

	cout << "[Received Packet] Type: " << msg_type << " Size: " << (int) payload_size << " Payload: ";
	for(int j = 0; j < payload_size; j++) {
		cout << buf[0][j] << " ";
	}
	cout << "\n";

	return ret;
}

/* this function is run by the second thread */
void *inc_x(void *x_void_ptr)
{
	int socket = *((int*) x_void_ptr);
	cout << "Thread de recepcion de comandos " << socket << "\n";

	for (;;) {
	   bzero(recv_buffer,256);
		int n = read( socket, recv_buffer, 255 );

		cout << "[Received Command] " << recv_buffer[0] << "\n";

		if(n == 0 || recv_buffer[0] == 'S' || recv_buffer[0] == 0)
		{
			cout << "Client closed conenction \n";
			staph = 1;
			return NULL;
		}

		if (n < 0) {
		   perror("ERROR reading from socket");
		   exit(1);
		}

		send_request(recv_buffer[0]);

	}

	/* the function must return something - NULL will do */
	return NULL;
}

int main() {

	int sockfd, newsockfd, portno, clilen;
   char buffer[256];
   struct sockaddr_in serv_addr, cli_addr;

   /* First call to socket() function */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);

   if (sockfd < 0) {
	  perror("ERROR opening socket");
	  exit(1);
   }
   cout << "Opened Socket\n";
   /* Initialize socket structure */
   bzero((char *) &serv_addr, sizeof(serv_addr));
   portno = 5001;

   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(portno);

   /* Now bind the host address using bind() call.*/
   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
	  perror("ERROR on binding");
	  exit(1);
   }
   cout << "Binded Socket to host address\n";
   /* Now start listening for the clients, here process will
	  * go in sleep mode and will wait for the incoming connection
   */

   listen(sockfd,1);
   clilen = sizeof(cli_addr);
   cout << "Listening to incoming connections...\n";
   /* Accept actual connection from the client */
   while(1) {
	   newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, (unsigned int *)&clilen);

	  if (newsockfd < 0) {
	  perror("ERROR on accept");
	  exit(1);
	  }

	  cout << "Connection established with client\n";
	  staph = 0;
	  /* If connection is established then start communicating */
	  bzero(buffer,256);

	  /* this variable is our reference to the second thread */
	  pthread_t inc_x_thread;

	  /* create a second thread which listens for incoming commands */
	  if(pthread_create(&inc_x_thread, NULL, inc_x, &newsockfd)) {
	   fprintf(stderr, "Error creating thread\n");
	   return 1;
	  }

	// Inicializar led conectado a GPIO y controlador de I2C

	d_pin = new mraa::Gpio(3, true, true);

	i2c = new mraa::I2c(0);
	i2c->address(8);

	float* data;


	// Indefinidamente
	while (staph == 0) {
		// Solicitamos los datos al Arduino
		send_request('E');
		// Recivimos los datos y los retransmitimos por el socket
		staph = receive_response(&data, newsockfd);

		// Limpiar buffer de datos
		free(data);

		// Forzar la salida de stdout
		fflush(stdout);

		//sleep(2);

   	   }
   }

    return 0;
}
