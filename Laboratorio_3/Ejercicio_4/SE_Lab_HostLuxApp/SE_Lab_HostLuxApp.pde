
/**
 * Ejemplo de skecth Processing para el desarrollo del Laboratorio.
 * Sistemas Embebidos - 1º Cuatrimestre de 2016
 *
 * Este skecth implementa un programa en el host que tiene la capacidad de
 * graficar funciones que evolucionan en el tiempo y además permite la
 * presentación de datos simples mediante Labels, y la captura de eventos
 * del usuario mediante la implementación de botones simples.
 *
 * La aplicación divide la ventana en 2 regiones, una de dibujado y otra 
 * donde se ubican los botones y etiquetas de información.
 *
 * Autor: Sebastián Escarza
 * Sistemas Embebidos - 1ºC de 2016.
 */

import processing.net.*;

Client cliente;
String data;

// Declaraciones para graficar funciones...
int cosVal;
int cantValues;
ScrollingFcnPlot f1,f2,lux,avg,min,max;

float avg_val=50,lux_val=25, min_val=10, max_val=100;
//Botones...
RectButton rectBtn1, rectBtn2;

RectButton A2, Right, Left, Down, Up;

PFont myFont;

//Etiquetas textuales...
boolean alert = false;
boolean prealert = false;
Label lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, lbl11;
Label yVal1,yVal2,yVal3;

//Ventana y viewports...
int pgFunctionViewportWidth = 680;
int pgControlViewportWidth = 250;
int pgViewportsHeight = 240;

void settings () {

    size(pgFunctionViewportWidth+pgControlViewportWidth, pgViewportsHeight);
}

void setup() {

  //Se define el tamaño de la ventana de la aplicación... 
  //size(pgFunctionViewportWidth+pgControlViewportWidth, pgViewportsHeight);
  cliente = new Client(this, "192.168.1.49", 5001);
  //Se inicializan los arreglos para almacenar las funciones...
  cantValues = pgFunctionViewportWidth;
  //f1 = new ScrollingFcnPlot(cantValues,color(100,0,0),-1,1);
  //f2 = new ScrollingFcnPlot(cantValues,color(0,100,0),0,height);
  lux = new ScrollingFcnPlot(cantValues,color(0,255,255),0,120);
  avg = new ScrollingFcnPlot(cantValues,color(0,255,0),0,120);
  min = new ScrollingFcnPlot(cantValues,color(0,0,255),0,120);
  max = new ScrollingFcnPlot(cantValues,color(255,0,0),0,120);

  //Se inicializan los botones... "▲ ▼◀ ▶"
  Up = new     RectButton(pgFunctionViewportWidth+60+45,10,40,40,color(102),color(50),color(255),"▲");
  Down = new   RectButton(pgFunctionViewportWidth+60+45,60,40,40,color(102),color(50),color(255),"▼");
  Left = new   RectButton(pgFunctionViewportWidth+10+45,60,40,40,color(102),color(50),color(255),"◀");
  Right = new  RectButton(pgFunctionViewportWidth+110+45,60,40,40,color(102),color(50),color(255),"▶");
  A2 = new     RectButton(pgFunctionViewportWidth+10+45,110,140,40,color(102),color(50),color(255),"A2");
  
  //Se inicializan los labels...
  lbl1 = new Label(pgFunctionViewportWidth+10,110+40,color(255),"Status: ");
  lbl2 = new Label(pgFunctionViewportWidth+60,110+40,color(255),"-");
  //LUX
  lbl3 = new Label(pgFunctionViewportWidth+10,130+40,color(255),"L.Actual: ");
  lbl4 = new Label(pgFunctionViewportWidth+70,130+40,color(0,255,255),"10l");
  //MAX
  lbl5 = new Label(pgFunctionViewportWidth+10+130,150+40,color(255),"L.Max: ");
  lbl6 = new Label(pgFunctionViewportWidth+70+130,150+40,color(255,0,0),"10l");
  //MIN
  lbl7 = new Label(pgFunctionViewportWidth+10+130,130+40,color(255),"L.Min: ");
  lbl8 = new Label(pgFunctionViewportWidth+70+130,130+40,color(0,0,255),"10l");
  //promedio
  lbl9 = new Label(pgFunctionViewportWidth+10,150+40,color(255),"L.Prom: ");
  lbl10 = new Label(pgFunctionViewportWidth+70,150+40,color(0,255,0),"10l");
  
  lbl11 = new Label(pgFunctionViewportWidth+10,210+40,color(255,0,0),"ALERTA!!!");
  
  yVal1 = new Label(10,5,color(255),"120L");
  yVal2 = new Label(10,(pgViewportsHeight-20)/2,color(255),"60L");
  yVal3 = new Label(10,pgViewportsHeight-25,color(255),"0L");
  
  //Inicializa el font de la GUI...
  myFont = createFont("FFScala", 14);
  textFont(myFont);
  
}

float convert(char entera, char decimal){
   
  return entera - 1 + (decimal - 1)/100.0f; 
}

void up_pressed(){
  lbl2.caption = "UP";
  cliente.write("K");
}
void down_pressed(){
  lbl2.caption = "DOWN";
  cliente.write("L");
}
void left_pressed(){
  lbl2.caption = "LEFT";
  cliente.write("M");

}
void right_pressed(){
  lbl2.caption = "RIGHT";
  cliente.write("N");

}
void a2_pressed(){
  lbl2.caption = "A2";
  cliente.write("O");

}

boolean keyRel = false;

void draw() {

  data = cliente.readString();
  if(data!=null){
    //lbl1.setString(data);
    lux_val = convert(data.charAt(0),data.charAt(1));
    min_val = convert(data.charAt(2),data.charAt(3));
    max_val = convert(data.charAt(4),data.charAt(5));
    avg_val = convert(data.charAt(6),data.charAt(7));
    lbl4.caption = ""+lux_val+"l";
    lbl6.caption = ""+max_val+"l";
    lbl8.caption = ""+min_val+"l";
    lbl10.caption = ""+avg_val+"l";
  }
  //Se actualizan las funciones de ejemplo (f1: coseno(x), f2: función que depende de la posición Y del mouse)
  float amount = map(cosVal, 0, cantValues, 0, 2*PI);
  //f1.updateValue(cos(amount));
  //f2.updateValue(height-mouseY);
  
  min.updateValue(min_val);
  max.updateValue(max_val);
  avg.updateValue(avg_val);
  lux.updateValue(lux_val);
  
  
  
  //Se incrementa el ángulo de la función coseno...
  cosVal = (cosVal + 1) % cantValues;
  
  //Permite expermientar con la velocidad de scroll al actualizar más lentamente los valores...
  //delay(50);
     
  //Rendering de la interface...
  background(125);
  stroke(0);
  noFill();
 
  //Dibuja las funciones...
  //f1.displayIntoRect(30,10,pgFunctionViewportWidth-10,pgViewportsHeight-10);
  //f2.displayIntoRect(30,10,pgFunctionViewportWidth-10,pgViewportsHeight-10);
  min.displayIntoRect(30,10,pgFunctionViewportWidth-10,pgViewportsHeight-10);
  max.displayIntoRect(30,10,pgFunctionViewportWidth-10,pgViewportsHeight-10);
  avg.displayIntoRect(30,10,pgFunctionViewportWidth-10,pgViewportsHeight-10);
  lux.displayIntoRect(30,10,pgFunctionViewportWidth-10,pgViewportsHeight-10);
  //Procesa eventos de MouseOver...
  Up.update();
  Down.update();
  Left.update();
  Right.update();
  A2.update();
  //Procesa las entradas (botones)
  if(mousePressed) {
    if(Up.pressed()) {
      Up.currentcolor = color(0,100,0);
      lbl2.caption = "UP";
      prealert = true;
    } 
    else if(Down.pressed()) {
      Down.currentcolor = color(0,100,0);
      lbl2.caption = "DOWN";
      prealert = true;
    }
    else if(Left.pressed()) {
      Left.currentcolor = color(0,100,0);
      lbl2.caption = "LEFT";
      prealert = true;
    } 
    else if(Right.pressed()) {
      Right.currentcolor = color(0,100,0);
      lbl2.caption = "RIGHT";
      prealert = true;
    }
    else if(A2.pressed()) {
      A2.currentcolor = color(0,100,0);
      lbl2.caption = "A2";
      prealert = true;
    }
  }
  
  
  if(keyRel) {
    if(keyCode == UP) {
      Up.currentcolor = color(0,100,0);
      lbl2.caption = "UP";
      prealert = true;
    } 
    else if(keyCode == DOWN) {
      Down.currentcolor = color(0,100,0);
      lbl2.caption = "DOWN";
      prealert = true;
    }
    else if(keyCode == LEFT) {
      Left.currentcolor = color(0,100,0);
      lbl2.caption = "LEFT";
      prealert = true;
    } 
    else if(keyCode == RIGHT) {
      Right.currentcolor = color(0,100,0);
      lbl2.caption = "RIGHT";
      prealert = true;
    }
    else if(keyCode == ' ') {
      A2.currentcolor = color(0,100,0);
      lbl2.caption = "A2";
      prealert = true;
    }
  }
  
  
  //Dibuja el eje X y el recuadro de los gráficos...
  stroke(0);
  line(30, pgViewportsHeight/2, pgFunctionViewportWidth-10, pgViewportsHeight/2);
  rect(30,10,pgFunctionViewportWidth-40,pgViewportsHeight-20);
  
  //Se dibujan los botones...
  Up.display();
  Down.display();  
  Left.display();
  Right.display();
  A2.display();
  
  //Se dibuja texto adicional...(labels, etc)
  lbl1.display();
  lbl2.display();
  lbl3.display();
  lbl4.display();
  lbl5.display();
  lbl6.display();
  lbl7.display();
  lbl8.display();
  lbl9.display();
  lbl10.display();

  if (alert) lbl11.display();
  
  yVal1.display();
  yVal2.display();
  yVal3.display();  
}

void mouseReleased()
{
  //Si se pulsó algún botón y se completa el click, se hace el toggle sobre la etiqueta de alerta...
  
   if(Up.pressed()) {
      up_pressed();
    } 
    else if(Down.pressed()) {
       down_pressed();
    }
    else if(Left.pressed()) {
      left_pressed();

    } 
    else if(Right.pressed()) {
      right_pressed();

    }
    else if(A2.pressed()) {
      a2_pressed();

    }
}

void keyReleased(){
  //if(key == CODED){
    if (keyCode == UP) {
      up_pressed();

    }
    if (keyCode == DOWN) {
      down_pressed();
    }
    if (keyCode == LEFT) {
      left_pressed();

    }
    if (keyCode == RIGHT) {
      right_pressed();

    }
    if (keyCode == ' ') {
      a2_pressed();
    }
    keyRel = true;
  //}
}


void exit() {
  //put code to run on exit here

  cliente.write("S");
  delay(200);
  super.exit();
}
