// Wire Slave Sender
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Sends data as an I2C/TWI slave device
// Refer to the "Wire Master Reader" example for use with this

// Created 29 March 2006

// This example code is in the public domain.


#include <Wire.h>


void setup() {
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
}

void loop() {
  delay(100);
}
String output = "122.4456";
int len = 8;
// function that executes whenever data is requested by master
// this function is registered as an event, see setup()

int index = 0;

void requestEvent() {
  const char* inicio_fin = "$";
  const char* tipo = "A";
  char tam[2];
  tam[0] = 6;
  tam[1] = 0;
  char mensaje[9];
  mensaje[8] = 0;
  for(int i = 0; i<len; i++){
      mensaje[i] = output[i];
  }
  if(index == 0)
    Wire.write(inicio_fin);
  if(index == 1)
    Wire.write(tam);
  if(index == 2)
    Wire.write(mensaje);
  if(index == 3)
    Wire.write(tipo);
  if(index == 4)
    Wire.write(inicio_fin);

   index = (index+1)%5;
  // respond with message of 6 bytes
  // as expected by master
}

