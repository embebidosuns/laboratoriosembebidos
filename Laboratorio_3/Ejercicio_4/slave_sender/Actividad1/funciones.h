/*
 * mensajes.h
 *
 * Created: 09/10/2018 15:11:47
 *  Author: Mayu
 */ 


#ifndef FUNCIONES_H_
#define FUNCIONES_H_
#include "Arduino.h"

unsigned char parte_entera(double input);

unsigned char parte_decimal(double input);

void convert_string(int8_t entera,int8_t decimal, char** toRet);

#endif /* MENSAJES_H_ */