#include "Arduino.h"
#include "adc.h"
#include "mensajes.h"

#ifndef keyboard_h
#define keyboard_h

#define TECLA_UP		0
#define TECLA_DOWN		1
#define TECLA_LEFT		2
#define TECLA_RIGHT		3
#define TECLA_SELECT	4
#define BOTON_A2		5
#define BOTON_A3		6
#define BOTON_A4		7	 
#define BOTON_A5		8
  



		void keyboard_init();
		void keyboard_loop();
		void key_down_callback(void (*handler)(), int tecla);
		void key_up_callback(void (*handler)(), int tecla);
		void get_key(int input);
		void set_aux_key_call(void (*call)(int));

		void virtual_key_down(char tipo);
#endif

