#include "LiquidCrystal.h"

#ifndef display_h
#define display_h

#define LA 0
#define LM 1
#define LP 2
#define AD 3
#define CANT_MODOS 4



void clear(int line);
void display(String linea0, String linea1);

LiquidCrystal* getLCD();

#endif

