#include "display.h"
#include "LiquidCrystal.h"

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

void clear(int line) {
	lcd.setCursor(0, line);
	lcd.print("                 ");
	
}


void display(String linea0, String linea1)
{
	clear(0);
	clear(1);
	
	lcd.setCursor(0, 0);
	lcd.print(linea0);

	lcd.setCursor(0, 1);
	lcd.print(linea1);
}

LiquidCrystal* getLCD(){
	return &lcd;
}

