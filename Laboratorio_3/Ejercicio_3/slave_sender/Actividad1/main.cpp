/*
 LCD1602 Keypad Shield 1.0 Test Sketch - LiquidCrystal Library
 
 Este sketch demuestra el uso del LCD1602 Key Shield 1.0.
 Para ello se toman los pulsos de los botones mediante la entrada
 analógica AD0 y, mediante la librería LiquidCrystal de Arduino 1.0,
 se muestra en el display la tecla pulsada. La librería LiquidCrystal
 permite controlar cualquier display LCD compatible con el 
 controlador Hitachi HD44780.
 
 La configuración de la Librería se realiza en base al esquemático
 del shield.
 
 Este ejemplo está basado en un ejemplo provisto con la documentación del
 LCD1602 Keypad Shield, el cuál ha sido adaptado en base a los ejemplos 
 de la librería LiquidCrystal de Arduino.
 
 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 http://arduino.cc/en/Reference/LiquidCrystal
 
 Adaptación por Sebastián Escarza.
 */

#include "Arduino.h"
// include the library code:
#include "LiquidCrystal.h"
#include "keyboard.h"
#include "adc.h"
#include "analog_to_lux.h"
#include "Wire.h"
#include "mensajes.h"
#include "display.h"
#include "funciones.h"
#define LA 0
#define LM 1
#define LP 2
#define AD 3
#define CANT_MODOS 4
#define SAMPLES 200

volatile bool first_round_flag = false;
// initialize the library with the numbers of the interface pins


double max_lux = -1;
double min_lux = 2000;
double avg_lux = -1;
double samples[SAMPLES];
int index = -1;
int count = 0;


volatile int analogread;//guarda el valor que entrega el sensor

int estadoActual = 0;//LA

volatile int interrupt_count = 0;//como no alcanza el prescaler se usa una variable contador
volatile int disp = 0;//para actualizar el display
volatile bool add_flag = false;
volatile bool disp_flag = false;

int reset_count = 0;

byte back_light[5] = {51, 102, 153, 204, 255};
byte back_light_porcent[5] = {20, 40, 60, 80, 100};
int back_light_actual = 3;
// these constants won't change.  But you can change the size of
// your LCD using them:
const int numRows = 2;
const int numCols = 16;

// Obtener la ultima muestra
double get_sample() {
	if(index > -1 && index < SAMPLES)
		return samples[index];
	else
		return 0;
}

void callback(int a){
	analogread = a;
}

// Agrega una muestra al arreglo circular
void add_sample() {
	if(first_round_flag){
		double lux = analog_to_lux(analogread);
		if(lux<min_lux)
		min_lux = lux;
		if(lux>max_lux)
		max_lux = lux;
		count = min(SAMPLES, count + 1);
		index = (index + 1) % SAMPLES;
		samples[index] = lux;
	}
}



void update_avg_lux() {
	avg_lux = 0;
	for (int i = 0; i < count; i++)
		avg_lux += samples[i];
	avg_lux = avg_lux / count;
}



void displayMode(){
	if(estadoActual==LA){
		display("Modo LA","Lux: ");
		getLCD()->print(get_sample());
	}
	if(estadoActual==LM){
		clear(0);
		clear(1);
		getLCD()->setCursor(0,0);
		getLCD()->print("Min: ");
		getLCD()->print(min_lux);
		getLCD()->setCursor(0,1);
		getLCD()->print("Max: ");
		getLCD()->print(max_lux);
		
	}
	if(estadoActual==LP){
		display("Modo LP","Prom: ");	
		update_avg_lux();
		getLCD()->print(avg_lux);
	}
	if(estadoActual==AD){
		display("Modo AD","");
		clear(1);
		getLCD()->setCursor(0,1);
		//getLCD()->print(back_light_actual);
		getLCD()->print("Brillo: ");
		getLCD()->print(back_light_porcent[back_light_actual]);
		getLCD()->print(" %");
	}
	
}

//para los eventos que no usamos se inicializa el driver de teclado con una funcion vacia 
void dummyFunction(){
	return;
}

void onKeyDown_Up(){
	if(estadoActual == AD){
		reset_count = 0;
		back_light_actual++;
		if(back_light_actual > 4){
			back_light_actual = 4;
		}
		else{
			analogWrite(10,back_light[back_light_actual]);
		}
		//analogWrite(10,back_light[back_light_actual]);
	}
}

void onKeyDown_Down(){
	
	if(estadoActual == AD){
		reset_count= 0;
		back_light_actual --;
		if(back_light_actual == -1){
			back_light_actual = 0;
		}
		else{
			analogWrite(10,back_light[back_light_actual]);
		}
		
	}
	/**/
}

void onKeyDown_Left(){
	if(first_round_flag){
		reset_count = 0;
		estadoActual = estadoActual - 1;//si estadoActual-1 < 0 le asigno 3, sino le resto
		if(estadoActual == -1)
			estadoActual = CANT_MODOS - 1;
	}
	//displayMode();
}

void onKeyDown_Right(){
		if(first_round_flag){
			reset_count = 0;
			estadoActual = (estadoActual + 1) % CANT_MODOS;
		}
	//displayMode();
}

void onKeyDown_A2() {
	
	display("Reset","");
	estadoActual = LA;
	delay(1000);
	reset_count = 0;
	disp = 0;
	disp_flag = false;
	add_flag = false;
	max_lux = -1;
	min_lux = 2000;
	index = -1;
	count = 0;
	first_round_flag = false;
	interrupt_count = 0;
	
}

int indexForSend = 0;
int indexForSendError = 0;
char comando_recibido = ESC_CHAR;

char* mensaje = "..........";
char* tipo_msg = " ";
char* inicio_msg = "$";
char* fin_msg = " ";

void requestEvent() {
	
	if(comando_recibido!=0){
		
		if(indexForSend == 0){
			Wire.write(inicio_msg);
			
		}
		if(indexForSend == 1){
			char tam[2];
			
			
			if(comando_recibido == OBTENER_TODO){
				tam[0] = 4;
			}
			else{
				tam[0] = 1;
			}
			if(comando_recibido==MSG_TECLA_A2
				|| comando_recibido== MSG_TECLA_DOWN
				|| comando_recibido==MSG_TECLA_LEFT
				|| comando_recibido==MSG_TECLA_RIGHT
				|| comando_recibido==MSG_TECLA_UP)
			{
				tam[0] = 0;
			}
			tam[1] = 0;
			
			Wire.write(tam);
		}
		if(indexForSend == 2){
			update_avg_lux();
			double lux = samples[index];
			double min = min_lux;
			double max = max_lux;
			double avg = avg_lux;
			if(comando_recibido == OBTENER_LUX){
				mensaje[0] = parte_entera(lux);
				mensaje[1] = parte_decimal(lux);
				mensaje[2] = ESC_CHAR;
				/*
				char* aux = (char*)malloc(sizeof(char)*10);
				convert_string(mensaje[0],mensaje[1], (char**)&aux);
				display("Enviando LUX",aux);
				reset_count = 0;
				estadoActual = -10;
				free(aux);
				/**/
			}
			if(comando_recibido == OBTENER_MAX){
				mensaje[0] = parte_entera(max);
				mensaje[1] = parte_decimal(max);
				mensaje[2] = ESC_CHAR;
				/*
				char* aux = (char*)malloc(sizeof(char)*10);
				convert_string(mensaje[0],mensaje[1], (char**)&aux);
				display("Enviando MAX",aux);
				reset_count = 0;
				//estadoActual = -10;
				free(aux);
				/**/
			}
			if(comando_recibido == OBTENER_MIN){
				mensaje[0] = parte_entera(min);
				mensaje[1] = parte_decimal(min);
				mensaje[2] = ESC_CHAR;
				/*
				char* aux = (char*)malloc(sizeof(char)*10);
				convert_string(mensaje[0],mensaje[1], (char**)&aux);
				display("Enviando MIN",aux);
				reset_count = 0;
				//estadoActual = -10;
				free(aux);
				/**/
			}
			if(comando_recibido == OBTENER_PROM){
				
				mensaje[0] = parte_entera(avg);
				mensaje[1] = parte_decimal(avg);
				mensaje[2] = ESC_CHAR;
				/*
				char* aux = (char*)malloc(sizeof(char)*10);
				convert_string(mensaje[0],mensaje[1], (char**)&aux);
				display("Enviando PROM",aux);
				reset_count = 0;
				//estadoActual = -10;
				free(aux);
				/**/
			}
			if(comando_recibido == OBTENER_TODO){
				mensaje[0] = parte_entera(lux);
				mensaje[1] = parte_decimal(lux);
				mensaje[2] = parte_entera(min);
				mensaje[3] = parte_decimal(min);
				mensaje[4] = parte_entera(max);
				mensaje[5] = parte_decimal(max);
				mensaje[6] = parte_entera(avg);
				mensaje[7] = parte_decimal(avg);
				mensaje[8] = ESC_CHAR;
				/*
				char* aux = (char*)malloc(sizeof(char)*10);
				convert_string(mensaje[6],mensaje[7],(char**) &aux);

				display("Enviando TODO",aux);
				reset_count = 0;
				//estadoActual = -10;
				free(aux);
				/**/
			}
			if(comando_recibido==MSG_TECLA_A2 
				|| comando_recibido== MSG_TECLA_DOWN
				|| comando_recibido==MSG_TECLA_LEFT
				|| comando_recibido==MSG_TECLA_RIGHT
				|| comando_recibido==MSG_TECLA_UP)
			{
				
				mensaje[0] = 0;
				mensaje[1] = ESC_CHAR;
				
					
			}
			Wire.write(mensaje);

		}
		if(indexForSend == 3){
			if(comando_recibido == OBTENER_LUX){
				tipo_msg[0] = RESPONDER_LUX;
				
			}
			if(comando_recibido == OBTENER_MAX){
				tipo_msg[0] = RESPONDER_MAX;
				
			}
			if(comando_recibido == OBTENER_MIN){
				tipo_msg[0] = RESPONDER_MIN;
				
			}
			if(comando_recibido == OBTENER_PROM){
				tipo_msg[0] = RESPONDER_PROM;
				
			}
			if(comando_recibido == OBTENER_TODO){
				tipo_msg[0] = RESPONDER_TODO;
				
			}
			if(comando_recibido==MSG_TECLA_A2
				|| comando_recibido== MSG_TECLA_DOWN
				|| comando_recibido==MSG_TECLA_LEFT
				|| comando_recibido==MSG_TECLA_RIGHT
				|| comando_recibido==MSG_TECLA_UP)
			{
				tipo_msg[0] = comando_recibido;
			}
			
			Wire.write(tipo_msg);

		}
		if(indexForSend == 4){
			
			comando_recibido = ESC_CHAR;
			Wire.write(fin_msg);
		}
		
		indexForSend = (indexForSend+1)%5;
	}
	else{
		
		if(indexForSendError==0){
			Wire.write(inicio_msg);
		}
		if(indexForSendError==1){
			char tam[2];
			tam[0] = 0;
			tam[1] = 0;
			Wire.write(tam);
		}
		if(indexForSendError==2){
			tipo_msg[0] = ERROR_MSG;
			Wire.write(tipo_msg);
		}
		if(indexForSendError==3){
			Wire.write(fin_msg);
		}
		indexForSendError = (indexForSendError+1)%4;

	}
	/**/
	// respond with message of 6 bytes
	// as expected by master
}

void receiveEvent(int input){
	char* str = "    ";
	str[3]=0;
	char c;
	int i = 0;
	//char de inicio
	c = Wire.read();
	str[0] = c;
	//if(c != '$')
		//return;
	
	//tamaño debe ser 0
	c = Wire.read();
	//str[1] = c;
	str[1] = '&';
	int tam = (int) c;
	//if(tam!=0)
		//return;
	
	//tipo
	c = Wire.read();
	str[2] = c;
	
	if (c==OBTENER_TODO || c==OBTENER_LUX || c==OBTENER_MIN || c==OBTENER_MAX || c==OBTENER_PROM )
		comando_recibido = c;
	else if(c==MSG_TECLA_A2 || c== MSG_TECLA_DOWN || c==MSG_TECLA_LEFT ||c==MSG_TECLA_RIGHT || c==MSG_TECLA_UP){
		
		virtual_key_down(c);
		comando_recibido = c;
	}
	/**/
	
	c = Wire.read();
	str[3] = c;
	
}




struct adc_cfg adc_cfg = { 1, callback};

void setup() {
	//TODO aca hacer un display
	getLCD()->begin(numCols,numRows);
	analogWrite(10,back_light[back_light_actual]);
	
	Wire.begin(8);                // join i2c bus with address #8
	Wire.onRequest(requestEvent); // register event
	Wire.onReceive(receiveEvent);
	inicio_msg[0] = INIT_CHAR;
	fin_msg[0] = END_CHAR;
	// TIMER 1 for interrupt frequency 16 Hz:
	cli(); // stop interrupts
	TCCR2A = 0; // set entire TCCR1A register to 0
	TCCR2B = 0; // same for TCCR1B
	TCNT2  = 0; // initialize counter value to 0
	// set compare match register for 16 Hz increments
	OCR2A = 155; // = 16000000 / (1024 * 16.66666 * 6) - 1 (must be <65536)
	// turn on CTC mode
	TCCR2A |= (1 << WGM02);
	// Set CS12, CS11 and CS10 bits for 1024 prescaler
	TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20);
	// enable timer compare interrupt
	TIMSK2 |= (1 << OCIE2A);
	/**/
	
	adc_init(&adc_cfg);
	//set_debug_callback(keyboard_debug_callback);
	keyboard_init();
	
	key_down_callback(onKeyDown_Up, TECLA_UP);
	key_up_callback(dummyFunction, TECLA_UP);
	key_down_callback(onKeyDown_Down, TECLA_DOWN);
	key_up_callback(dummyFunction, TECLA_DOWN);
	key_down_callback(onKeyDown_Left, TECLA_LEFT);
	key_up_callback(dummyFunction, TECLA_LEFT);
	key_down_callback(onKeyDown_Right, TECLA_RIGHT);
	key_up_callback(dummyFunction, TECLA_RIGHT);
	key_down_callback(dummyFunction, TECLA_SELECT);
	key_up_callback(dummyFunction, TECLA_SELECT);
	key_down_callback(onKeyDown_A2, BOTON_A2);
	key_up_callback(dummyFunction, BOTON_A2);
	key_down_callback(dummyFunction, BOTON_A3);
	key_up_callback(dummyFunction, BOTON_A3);
	key_down_callback(dummyFunction, BOTON_A4);
	key_up_callback(dummyFunction, BOTON_A4);
	key_down_callback(dummyFunction, BOTON_A5);
	key_up_callback(dummyFunction, BOTON_A5);
	

	
	sei(); // allow interrupts
	estadoActual = LA;
}

void loop()
{
	keyboard_loop();
	
	if(add_flag){
		add_sample();
		add_flag = false;
	}
	if(disp_flag){
		displayMode();
		disp_flag = false;
	}
}

ISR(TIMER2_COMPA_vect){
	interrupt_count = (interrupt_count+1) % 6;
	if(interrupt_count == 0){
		reset_count++;
		add_flag = true;
		disp = (disp + 1) % 10;
		if(disp == 0){
			disp_flag = true;
			if(!first_round_flag)
			first_round_flag = true;
		}
		if(reset_count>=300){
			estadoActual = LA;
			disp_flag = true;
			reset_count = 0;
		}
	}
}

