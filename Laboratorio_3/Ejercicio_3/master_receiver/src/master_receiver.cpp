#include "mraa.hpp"

#include <iostream>
#include "mensajes.h"
using namespace std;

mraa::Gpio* d_pin = NULL;

mraa::I2c* i2c;

uint8_t rx_tx_buf[16];
// Maximo 6 datos de 2 bytes c/u
//uint8_t payload[2 * 6];
uint8_t payload_size = 0;
uint8_t msg_type;
uint8_t last_byte;

void clear_buffer() {
	for(int j = 0; j < 16; j++) {
		rx_tx_buf[j] = 0;
	}
}

uint8_t read_byte() {
	uint8_t buffer[1];
	i2c->read(buffer, 1);
	return buffer[0];
}

void read_payload(uint8_t size, float* data) {
	clear_buffer();
	uint8_t buffer[size * 2];
	i2c->read(buffer, size * 2);

	int entera;
	int decimal;
	for(int j = 0; j < size; j++) {
		entera = (int) buffer[j * 2];//read_byte();
		decimal = (int) buffer[j * 2 + 1];//read_byte();
		data[j] = entera + decimal * .01f;
	}
}

void send_request(char type) {
	uint8_t* data = (uint8_t*) malloc(sizeof(uint8_t) * 4);
	data[0] = '$';
	data[1] = 0;
	data[2] = type;
	data[3] = '#';
	i2c->write(data, 4);
	cout << "[Sent Packet] Type: " << data[2] << " Size: " << (int) data[1] << "\n";
	free(data);
}

void receive_response(float** buf) {
	clear_buffer();
	cout << "Awaiting Response... \n";
	// Espero a leer un $
	while(rx_tx_buf[0] != '$') {
		//cout << "Awaiting Response... \n";
		i2c->read(rx_tx_buf, 1);
	}
	cout << "Got Response... \n";

	// Leer el Tama�o del Payload
	payload_size = read_byte();

	// Leo el Payload en funcion del tama�o
	*buf = (float*) malloc(sizeof(float) * payload_size);
	read_payload(payload_size, *buf);

	// Leo el tipo
	msg_type = read_byte();

	// Leo el fin
	last_byte = read_byte();

	//cout << "[Received Packet] Type: " << msg_type << " Size: " << (int) payload_size << " Payload: " << buf[0] << " " << buf[1] << " "  << buf[2] << " "  << buf[3] << "\n";
	cout << "[Received Packet] Type: " << msg_type << " Size: " << (int) payload_size << " Payload: ";
	for(int j = 0; j < payload_size; j++) {
		cout << buf[0][j] << " ";
	}
	cout << "\n";
}

int main() {
	// Inicializar led conectado a GPIO y controlador de I2C

	d_pin = new mraa::Gpio(3, true, true);

	i2c = new mraa::I2c(0);
	i2c->address(8);

	float* data;

    // Indefinidamente
    for (;;) {

    	// Apagar led y recibir por I2C
    	sleep(1);
    	d_pin->write(0);
    	//i2c->read(rx_tx_buf, 16);

    	char c = '?';
    	while( !(c >= 'A' && c <= 'E') && !(c >= 'K' && c <= 'O') ) {
    		cout << "Seleccione una opcion A-E para solicitar Datos. \n";
    		cout << "Seleccione una opcion K-O para enviar Comandos. \n";
			//cout << "    A) Obtener LUX\n";
			//cout << "    B) Obtener MAX\n";
			//cout << "    C) Obtener MIN\n";
			//cout << "    D) Obtener PROMEDIO\n";
			//cout << "    E) Obtener TODO\n";

			cout << "    A) Obtener LUX         K) Simular Tecla Up\n";;
			cout << "    B) Obtener MAX         L) Simular Tecla Down\n";
			cout << "    C) Obtener MIN         M) Simular Tecla Left\n";
			cout << "    D) Obtener PROMEDIO    N) Simular Tecla Right\n";
			cout << "    E) Obtener TODO        O) Simular Boton A2\n";

			cin >> c;
			cout << "Usted a seleccionado la opcion " << c << "\n";
    	}

    	send_request(c);
    	receive_response(&data);

    	// Limpiar buffer de datos
    	free(data);

    	// Luego de un segundo, encender led e imprimir por stdout
    	sleep(1);
    	d_pin->write(1);
    	//printf((char *) rx_tx_buf, "%s\n");

    	// Forzar la salida de stdout
    	fflush(stdout);
    }

    return 0;
}
