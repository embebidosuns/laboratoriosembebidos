/*
 LCD1602 Keypad Shield 1.0 Test Sketch - LiquidCrystal Library
 
 Este sketch demuestra el uso del LCD1602 Key Shield 1.0.
 Para ello se toman los pulsos de los botones mediante la entrada
 analógica AD0 y, mediante la librería LiquidCrystal de Arduino 1.0,
 se muestra en el display la tecla pulsada. La librería LiquidCrystal
 permite controlar cualquier display LCD compatible con el 
 controlador Hitachi HD44780.
 
 La configuración de la Librería se realiza en base al esquemático
 del shield.
 
 Este ejemplo está basado en un ejemplo provisto con la documentación del
 LCD1602 Keypad Shield, el cuál ha sido adaptado en base a los ejemplos 
 de la librería LiquidCrystal de Arduino.
 
 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 http://arduino.cc/en/Reference/LiquidCrystal
 
 Adaptación por Sebastián Escarza.
 */
#include <MySoftwareSerial.h>
#include "Arduino.h"
// include the library code:
#include "keyboard.h"
#include "keycodes.h"
//#include "adc.h"

volatile int interrupt_count = 0;//como no alcanza el prescaler se usa una variable contador
volatile int disp = 0;//para actualizar el display
volatile bool add_flag = false;
volatile bool disp_flag = false;

int reset_count = 0;

 SoftwareSerial mySerial(2,3); 
//para los eventos que no usamos se inicializa el driver de teclado con una funcion vacia 
void dummyFunction(){
  return;
}



void onKeyDown_Up(){
  //
  mySerial.write(UP_PRESS);
 // mySerial.print("UP PRESS\n"); 
}

void onKeyUp_Up(){
  //

  mySerial.write(UP_RELEASE);   
  //mySerial.print("UP RELEASE\n"); 
}

void onKeyDown_Down(){
  
  //
  
  mySerial.write(DOWN_PRESS);
  //mySerial.print("DOWN PRESS\n");
}

void onKeyUp_Down(){
  
  mySerial.write(DOWN_RELEASE);
  //mySerial.print("DOWN RELEASE\n");
}


void onKeyDown_Left(){
  
  //
  
  mySerial.write(LEFT_PRESS);
  //mySerial.print("LEFT PRESS\n");
}

void onKeyUp_Left(){
  mySerial.write(LEFT_RELEASE);
  
  //mySerial.print("LEFT RELEASE\n");
}


void onKeyDown_Right(){
  
  //
  mySerial.write(RIGHT_PRESS);
  //mySerial.print("RIGHT PRESS\n");
}

void onKeyUp_Right(){
  
  //
  mySerial.write(RIGHT_RELEASE);
  //mySerial.print("RIGHT Release\n");

}



void onKeyDown_A2() {
    mySerial.write(BOTON_PRESS);

}
  
void onKeyUp_A2() {
    mySerial.write(BOTON_RELEASE);

}
  
void setup() {
  
  Serial.begin(9600);
  mySerial.begin(9600);


  // TIMER 1 for interrupt frequency 16 Hz:
  cli(); // stop interrupts

  //set_debug_callback(keyboard_debug_callback);
  
  keyboard_init();
  
  key_down_callback(onKeyDown_Up, TECLA_UP);
  key_up_callback(onKeyUp_Up, TECLA_UP);
  key_down_callback(onKeyDown_Down, TECLA_DOWN);
  key_up_callback(onKeyUp_Down, TECLA_DOWN);
  key_down_callback(onKeyDown_Left, TECLA_LEFT);
  key_up_callback(onKeyUp_Left, TECLA_LEFT);
  key_down_callback(onKeyDown_Right, TECLA_RIGHT);
  key_up_callback(onKeyUp_Right, TECLA_RIGHT);
  key_down_callback(dummyFunction, TECLA_SELECT);
  key_up_callback(dummyFunction, TECLA_SELECT);
  key_down_callback(onKeyDown_A2, BOTON_A2);
  key_up_callback(onKeyUp_A2, BOTON_A2);
  key_down_callback(dummyFunction, BOTON_A3);
  key_up_callback(dummyFunction, BOTON_A3);
  key_down_callback(dummyFunction, BOTON_A4);
  key_up_callback(dummyFunction, BOTON_A4);
  key_down_callback(dummyFunction, BOTON_A5);
  key_up_callback(dummyFunction, BOTON_A5);
  
  /**/
  sei(); // allow interrupts
  

}
int state = 0;
void loop()
{
  keyboard_loop();
  /*
  if(mySerial.available() > 0){ // Checks whether data is comming from the serial port
    state = mySerial.read(); // Reads the data from the serial port
  }
 /**/
}
