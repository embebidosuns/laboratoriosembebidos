#ifndef adc_h
#define adc_h

  struct my_adc_cfg 
  {
	  // ADC Channel A0:A5
	  int channel;
	  void (*callback)(int,int);
  };


	int adc_add_config(struct my_adc_cfg *cfg);
	int adc_init(struct my_adc_cfg *cfg);
	void adc_loop();
	
	int set_channel(int channel);



	

#endif

