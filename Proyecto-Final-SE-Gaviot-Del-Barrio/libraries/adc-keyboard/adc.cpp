#include "Arduino.h"
#include "adc.h"
#define CANALES 6


volatile int adcReading[CANALES];

volatile bool adcDone;
struct my_adc_cfg *config[CANALES] ={NULL,NULL,NULL,NULL,NULL};

bool canales[CANALES] = {false,false,false,false,false,false};

int currentChannel;

int set_channel(int channel)
{
	//cli();
	int error = 1;
	if(channel<=5 && channel>=0 )
	{
		
		canales[channel] = true;
		
	}
	else error = 0;
	
	//iniciamos la conversion
	
	//sei();
	return error;
}

int adc_add_config(struct my_adc_cfg *cfg) {
	config[cfg->channel] = cfg;
	if (!set_channel(cfg->channel))
		return 0;
	return 1;
}

int adc_init(struct my_adc_cfg *cfg) 
{
	cli();
	if (cfg->channel < 0  || cfg->channel > 5 )
		return 0;
		
	
	adcDone = false;
	//thisDriver->config->callback(1);
	//ADMUX
	//ADMUX |= (1<<REFS0) | (1<<ADLAR);
	ADMUX = 0;

	
	if (!adc_add_config(cfg))
		return -1;
	currentChannel = cfg->channel;
	ADMUX = cfg->channel;
	ADMUX |= (1<<REFS0) | (1<<ADLAR);
	ADCSRA |= (1<<ADEN)|  (1<<ADSC) | (1<<ADIE);
	// The ADC is enabled by writing a '1' to the ADC Enable bit in the ADC Control and Status Register A (ADCSRA.ADEN).
	// Bit 7 � ADEN:?ADC Enable
	// Writing this bit to one enables the ADC. By writing it to zero, the ADC is turned off. Turning the ADC off
	// while a conversion is in progress, will terminate this conversion
	ADCSRA =  0;
	
	// Bits 2:0 � ADPSn:?ADC Prescaler Select [n = 2:0]
	// These bits determine the division factor between the system clock frequency and the input clock to the ADC.
	ADCSRA |= (1<<ADEN) | (0<<ADATE) | (1<<ADSC) | (1<< ADIE);
	//Enable- auto trigger - inicia conv - interrupt enable
	
	ADCSRA |= (1<<ADPS1) | (1<<ADPS2) | (1<<ADPS0);
	//Frecuencia del conversion = 125KHz
	//Como estoy con velocidad humana, pueder obtener precicion
	//con baja frecuencia
	
	ADCSRB = 0; //Free running mode
	
	sei();
	
	return 1;
}

int nextChannel(int index){
	for(int i = index + 1; i< CANALES; i++){
		if(canales[i]){
			return i;
		}
	}
	return 0;
}

int prevChannel(int index){
	for(int i = index - 1; i > -1; i--){
		if(canales[i]){
			return i;
		}
	}
	int mayor = 0;
	for(int i = 0; i<CANALES; i++){
		if(canales[i]){
			mayor = i;
		}
	}
	return mayor;
}
// Interrupcion de finalizacion de lectura ADC
ISR (ADC_vect)
{	
	
//if(readActual == 0){
	adcReading[currentChannel] = ADC>>6;
	//adcReading[0] = nextChannel(1);
	//adcReading[1] = nextChannel(2);
	
	currentChannel = nextChannel(currentChannel);

	ADMUX = currentChannel;
				
		
	ADMUX |= (1<<REFS0) | (1<<ADLAR);

	//}
	adcDone = true;
	ADCSRA |=  (1<<ADSC) ;
}

void adc_loop() 
{
	// Si la ultima lectura ya termino, procesarla
	if (adcDone)
	{
		//adcStarted = false;

		// Retornamos el valor por medio de la funcion callback
		//Serial.println (adcReading);
		for(int i = 0; i<CANALES; i++)
			if(config[i]!=NULL)
				config[i]->callback(adcReading[i],i);
		
			//currentChannel = nextChannel(currentChannel+1);
		//}
		

		adcDone = false;
	}
	
	// Si no estamos leyendo un valor, empezamos una nueva lectura
	//if (!adcStarted)
	//{
		//adcStarted = true;
		// Bit 6 � ADSC:?ADC Start Conversion
		// In Single Conversion mode, write this bit to one to start each conversion. In Free Running mode, write
		// this bit to one to start the first conversion. The first conversion after ADSC has been written after the ADC
		// has been enabled, or if ADSC is written at the same time as the ADC is enabled, will take 25 ADC clock
		// cycles instead of the normal 13. This first conversion performs initialization of the ADC.
		// ADSC will read as one as long as a conversion is in progress. When the conversion is complete, it returns
		// to zero. Writing zero to this bit has no effect.
		// Bit 3 � ADIE:?ADC Interrupt Enable
		// When this bit is written to one and the I-bit in SREG is set, the ADC Conversion Complete Interrupt is activated
		//ADCSRA |= bit (ADSC) | bit (ADIE);
	//}
}