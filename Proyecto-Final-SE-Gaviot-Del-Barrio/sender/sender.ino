   #define anchoPantalla 15
int anchoMapa;
int altoMapa;
char* mapa;



#include "keycodes.h"
#include "input.h"
#include "audio.h"
#include "fisica.h"
#include "MemoryCard.h"
// include the library code:
#include <LiquidCrystal.h>

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 3, en = 2, d4 = 7, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

byte corrimiento = 0;

#define F_CPU 16000000L
//long tiempo = 0;

char bufferChar[32];

int mapa_actual = 0;

unsigned char pos_puerta = 1;
int vidas = 3;
int anillos = 0;

int puntaje = 0;
int fin_nivel = 0;

void PantallaInicio_logica();
void play_game();
void PantallaFin_logica();
void enviar();
void reset();
void enviar_mapas();

void setup()
{
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(33, OUTPUT);
  digitalWrite(33, HIGH);
  pinMode(31, OUTPUT);
  
  // Begin the Serial at 9600 Baud
  Serial1.begin(9600);
  Serial2.begin(9600);
  Serial.begin(9600);
  //Serial.begin(57600);
  setup_music();
  
  delay(200);
  musicOFF();

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
}

void loop()
{
  PantallaInicio_logica();
  if (PantallaLevels_logica()){
    play_game();
    if(vidas>0)
      PantallaWin_logica();
    else 
      PantallaFin_logica();
    
  }
  limpiar_buffer_Joystick();
}

void reset_game_over(){
  reset();
  vidas = 3;
  puntaje = 0;
  pos_puerta = 1;
  fin_nivel = 0;
  anillos = 0;
}

void enviar_comando(char cmd){
  cmd++;
  bufferChar[0] = cmd;
  enviar();
}

void clearDisplay() {
  lcd.setCursor(0,0);
  lcd.print("                ");
  lcd.setCursor(0,1);
  lcd.print("                ");
}

void actualizarDisplay() {
  // Print a message to the LCD.
  clearDisplay();
  lcd.setCursor(0,0);
  lcd.print("Puntos: ");
  lcd.print(puntaje);
  lcd.setCursor(0,1);
  lcd.print("Vidas : ");
  lcd.print(vidas);
}

void limpiar_buffer_Joystick(){
  while(Serial2.available())
      Serial2.read();
}

bool PantallaLevels_logica(){
  clearDisplay();
  lcd.setCursor(0,0);
  lcd.print("LEYENDO DATOS");
  lcd.setCursor(0,1);
  lcd.print("DE MEMORIA...");
  
   if (!listarNiveles()) {
      Serial1.write(254);
      clearDisplay();
      lcd.setCursor(0,0);
      lcd.print("ERROR AL LEER");
      lcd.setCursor(0,1);
      lcd.print("MEMORIA SD");
      delay(4000);
      return false;    
   }

   delay(500);
   Serial1.write(BOTON_PRESS);

   //aca hay que limpiar el buffer del joystick
   limpiar_buffer_Joystick();
    
   enviar_mapas();
   long tiempo_actual = millis();
   while(A_button==0){

     clearDisplay();
     lcd.setCursor(0,0);
     lcd.print("NIVELES: ");
     lcd.print(cant_mapas);
     lcd.setCursor(0,1);
     lcd.print("ACTUAL: ");
     lcd.print(mapa_actual + 1);
   
    checkInput();
  

    if(up_button == 1){
        if(cant_mapas>5 && millis() - tiempo_actual > 500){
          tiempo_actual = millis();
          mapa_actual = (mapa_actual + 5)%10;
          mapa_actual = min(cant_mapas - 1, mapa_actual);
          enviar_mapas(); 
          Serial.println("Mapa seleccionado");   
          Serial.println(mapa_actual);
        }
    }
    
    
    if(down_button == 1){
         if(cant_mapas>5 && millis() - tiempo_actual > 500){
          tiempo_actual = millis();
          mapa_actual = (mapa_actual + 5)%10;
          mapa_actual = min(cant_mapas - 1, mapa_actual);
          enviar_mapas(); 
          Serial.println("Mapa seleccionado");   
          Serial.println(mapa_actual);
        }
    }

    if(left_button == 1){
      if(millis() - tiempo_actual > 500){
        tiempo_actual = millis();
        mapa_actual = (mapa_actual + cant_mapas - 1) % cant_mapas;
        enviar_mapas();   
        Serial.println("Mapa seleccionado");   
        Serial.println(mapa_actual);
      }
    }
    
    if(right_button == 1){
      if(millis() - tiempo_actual > 500){
        tiempo_actual = millis();
        mapa_actual = (mapa_actual +1) % cant_mapas;
        enviar_mapas();  
        Serial.println("Mapa seleccionado");   
        Serial.println(mapa_actual);
      }  
    }

    delay(50);
    digitalWrite(31, LOW);
   
  }
  A_button = 0;
  digitalWrite(31, HIGH);
  
  Serial1.write(BOTON_PRESS);

  delay(200);
  digitalWrite(31, LOW);
  
  clearDisplay();
   lcd.setCursor(0,0);
   lcd.print("NIVEL ");
   lcd.print(mapa_actual + 1);
   lcd.setCursor(0,1);
   lcd.print("CARGANDO...");
  
  mapa = cargarNivel(mapa_actual, altoMapa, anchoMapa);
    
  Serial.println();
  Serial.println("El mapa a sido cargado con exito");
  Serial.print("-ALTO: ");
  Serial.println(altoMapa);
  Serial.print("-ANCHO: ");
  Serial.println(anchoMapa);
  Serial.println();
  
  // Mostramos por Serial Monitor el mapa cargado
  for(int i = 0; i < altoMapa * anchoMapa; i++) {
    Serial.write(mapa[i] + '0');
    if(mapa[i]==3) 
      anillos ++;//cuento los anillos en esta primera pasada al mapa
    if ((i + 1) % anchoMapa == 0)
    {
      Serial.println();
    }
  }
  Serial.println("saliendo de modo LEVELS");

  delay(500);
  //envio el comando 
  //Serial1.write(BOTON_PRESS);
  return true;
}

void enviar_mapas(){
   digitalWrite(31, HIGH);
   Serial1.write(254);
   Serial1.write((char)cant_mapas + 1);
   Serial1.write((char)(mapa_actual + 1));
   
}

void play_game(){
  delay(200);
  musicOFF();

  actualizarDisplay();
  
  while(vidas>0 && fin_nivel == 0){
    checkInput();
    
    move();
    
    comprimirFrame();
  
    posicionarPelota();
  
    enviar();
      
    if (muerte == 1){
      reset();
      puntaje = puntaje-50;
      puntaje = max(0, puntaje);
      actualizarDisplay();
    }
  
    checkPinche();
    checkAnillo();
    
    if(anillos== 0 && pos_puerta > 1){
      checkPuerta();
    }
    
  }
  Serial.println("Saliendo del modo PLAY");
}

void PantallaInicio_logica(){
  // Print a message to the LCD.
  clearDisplay();
  lcd.setCursor(0,0);
  lcd.print(" A R D U I N O");
  lcd.setCursor(0,1);
  lcd.print("  B O U N C E");
  
  musicON();
  while(A_button==0){
    checkInput();
  }
  digitalWrite(31, HIGH);
  
  delay(50);
  Serial1.write(BOTON_PRESS);
  delay(200);
  digitalWrite(31, LOW);
  
  A_button = 0;
  Serial.println("saliendo de modo INICIO");
  limpiar_buffer_Joystick();
}

void PantallaFin_logica(){
  clearDisplay();
   lcd.setCursor(0,0);
   lcd.print("   GAME OVER!");
   lcd.setCursor(0,1);
   lcd.print("P: ");
   lcd.print(puntaje);
   lcd.print(" V: ");
   lcd.print(vidas);
   
  enviar_comando(254);
  playEffectFin();
  reset_game_over();
  long actualTime = millis();
  while(millis() - actualTime < 5000 && A_button==0){
    checkInput();
  }
  delay(50);
  //Serial.println("Salio");
  Serial1.write(BOTON_PRESS);
  A_button = 0;
  
}

void PantallaWin_logica(){

  clearDisplay();
   lcd.setCursor(0,0);
   lcd.print("NIVEL COMPLETADO");
   lcd.setCursor(0,1);
   lcd.print("  V: ");
   lcd.print(vidas);

   lcd.print(" P: ");
   lcd.print(puntaje);

    bufferChar[0] = 254;
    bufferChar[1] = 'x' + 1;
    bufferChar[2] = vidas + 1 + '0';
    bufferChar[3] = 1;
    
    
    
    int index = 4;
    int aux = 1;
    int maximo = 0;
    for(maximo=0;puntaje/aux > 0; aux *=10, maximo++);

    
    aux = puntaje;
    Serial.println(maximo);
    aux = puntaje;
    
    while(aux / 10 > 0 || maximo > 0){
      bufferChar[index] = aux / (int)pow(10,maximo-1) + '0' + 1;
      aux = (aux % (int)pow(10,maximo-1));
      maximo--;
      index++;
    }
   bufferChar[index-1] = '1';
    bufferChar[index ] = 1;

    enviar();
    playEffectWin();
    bufferChar[index ] = 0;

    Serial.println(&bufferChar[4]);
    
    Serial.println(puntaje);
    reset_game_over();
  while(A_button==0){
    checkInput();
  }
  delay(50);
  mapa_actual = (mapa_actual+1)%cant_mapas;
  Serial.println("Saliendo del modo WIN");
  //Serial.println("Salio");
  Serial1.write(BOTON_PRESS);
  A_button = 0;

  
  
  //pantalla de niveles
  
}



void reset()
{
  delay(700);
  vel[DOWN] = 0;
  vel[LEFT] = 0;
  vel[RIGHT] = 0;

  j = 0;

  x_pelota = 4;
  y_pelota = 15;

  if(muerte == 1) {
   vidas--; 
  }
  muerte = 0;
  //actualizarDisplay();
  
  Serial.println("reset");
}


void comprimirFrame()
{
  int y = 0;
  int x = 0;
  //j = 0;
  int i = 0;
  int k = 0;
  pos_puerta = 1;//la variable se actualiza al leer del mapa, sino debe ser 1
  
  for (y = 0; y < altoMapa; y++)
  {
    bufferChar[i] = 0;
    for (x = 0; x < anchoPantalla; x++)
    {
      if (mapa[y * anchoMapa + x + j] == 1)
      {
        bufferChar[i] |= 0b01;
      }
      else
      {
        if (mapa[y * anchoMapa + x + j] == 2)
        {
          bufferChar[i] |= 0b10;
        }
        if (mapa[y * anchoMapa + x + j] == 3)
        {
          bufferChar[i] |= 0b11;
        }
        if (mapa[y * anchoMapa + x + j] == 4)
        {
          
          pos_puerta = x;
          pos_puerta = pos_puerta<<4;
          pos_puerta |= y;
          pos_puerta ++;
        }
      }

      if (x % 4 < 3)
        bufferChar[i] = bufferChar[i] << 2;
      else
      {
        bufferChar[i]++;
        i++;
        bufferChar[i] = 0;
      }

      if (x == 14)
      {
        bufferChar[i]++;
        i++;
        bufferChar[i] = 0;
      }
    }
  }
}

byte flag_corrimiento = 0;

void posicionarPelota()
{

  bufferChar[28] = pos_puerta;

  bufferChar[29] = x_relativo * 2 + x_pelota % 2 + 1;
  bufferChar[30] = max(y_pelota + 1, 1);
  if(muerte == 0)
    bufferChar[31] = (x_pelota) % 4 + 1;
  else
    bufferChar[31] = 5;

}

void enviar()
{
  digitalWrite(31, HIGH);
  Serial1.write(bufferChar, 32); //Write the serial data
                   //corrimiento = (corrimiento + 2) % 16;
  delay(75);
  digitalWrite(31, LOW);
  delay(75);
}

void checkPinche()
{
  //colision UP
  if ((colision(x_pelota, y_pelota) == 2 || colision(x_pelota + 1, y_pelota) == 2))
  {
    muerte = 1;
    
    playEffectMuerte();//EFECTO MUERTE
  }

  //colision DOWN
  if ((colision(x_pelota, y_pelota + 8) == 2 || colision(x_pelota + 1, y_pelota + 8) == 2) && y_pelota % 8 != 0)
  {

    muerte = 1;
    playEffectMuerte();//EFECTO MUERTE
  }

  //colision right
  if (colision(x_pelota + 1, y_pelota) == 2)
  {

    muerte = 1;
    playEffectMuerte();//EFECTO MUERTE
  }

  //colision left
  if (colision(x_pelota - 1, y_pelota) == 2 && x_pelota % 2 != 0)
  {

    muerte = 1;
    playEffectMuerte();//EFECTO MUERTE
  }
}

void quitar_anillo(int x_pelota, int y_pelota)
{
  mapa[(y_pelota / 8) * anchoMapa + x_pelota / 2] = 0;
  anillos--;
  puntaje += 100;
  actualizarDisplay();
  playEffectAnillo();//EFECTO MUERTE
}

void checkAnillo()
{
  //colision UP
  if ((colision(x_pelota, y_pelota) == 3 || colision(x_pelota + 1, y_pelota) == 3))
  {
    if (colision(x_pelota, y_pelota) == 3)
      quitar_anillo(x_pelota, y_pelota);
    else
      quitar_anillo(x_pelota + 1, y_pelota);
  }

  //colision DOWN
  if ((colision(x_pelota, y_pelota + 8) == 3 || colision(x_pelota + 1, y_pelota + 8) == 3))
  {
    if (colision(x_pelota, y_pelota + 8) == 3)
      quitar_anillo(x_pelota, y_pelota + 8);
    else
      quitar_anillo(x_pelota + 1, y_pelota + 8);
  }

  //colision right
  if (colision(x_pelota + 1, y_pelota) == 3 || colision(x_pelota + 1, y_pelota + 8) == 3 && x_pelota % 2 != 0)
  {
    if (colision(x_pelota + 1, y_pelota) == 3)
      quitar_anillo(x_pelota + 1, y_pelota);
    else
      quitar_anillo(x_pelota + 1, y_pelota + 8);
  }

  //colision left
  if ((colision(x_pelota - 1, y_pelota) == 3 || colision(x_pelota - 1, y_pelota + 8) == 3) && x_pelota % 2 != 0)
  {
    if (colision(x_pelota - 1, y_pelota) == 3)
      quitar_anillo(x_pelota - 1, y_pelota);
    else
      quitar_anillo(x_pelota - 1, y_pelota + 8);
  }
}

void checkPuerta(){
  int x_puerta = pos_puerta>>4;
  int y_puerta = pos_puerta&0b1111;
  y_puerta--;
  
  if(y_pelota/8 == y_puerta && x_relativo == x_puerta
    || y_pelota/8 == y_puerta +1 && x_relativo == x_puerta
    || y_pelota/8  == y_puerta +1 && x_relativo  == x_puerta  +1
    || y_pelota/8  == y_puerta && x_relativo == x_puerta  +1
  ){
    
    fin_nivel = 1;
    
  }
  
}
