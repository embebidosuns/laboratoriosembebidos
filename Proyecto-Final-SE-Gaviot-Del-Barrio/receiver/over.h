//image generated from 2BITIMAGE - by Sandro Maffiodo
#define IMG_GAME_OVER_WIDTH 120
#define IMG_GAME_OVER_BWIDTH 30
#define IMG_GAME_OVER_HEIGHT 56
//data size=1680 bytes
const unsigned char img_game_over_data[VGAX_HEIGHT * VGAX_BWIDTH] PROGMEM = {
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  32,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 128,   0, 128,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2, 170, 128,  32,   0, 128,   8,   0,   0,  32,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   2,   0,   0,  10,  42, 128,  32,   2,   0,   8,   0,   0,  32,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   8, 170, 128,   8,   0, 128,   8,   0,  32,  40,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0, 128,  32,  10, 170,   0,   0,   0,   0,  32,   0,   0,   8,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0, 128,   8,  10, 168, 128,   0,   0,   0,  32,  42, 128,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2,   0,  34, 128,   2, 160,   0,   0, 162, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 128,  40, 128,  42,  42, 170,   0, 168, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,  32,   0,   0,   0,  42, 128, 170, 170,  42, 130, 138, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,  10,   0,   0,   0,  10, 170,  42, 138, 138, 170, 234, 168,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0, 160,   0, 170, 168, 170, 138, 170, 162, 175, 226,  40,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   8,   2, 170, 162, 170, 170, 170, 170, 171, 234, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  10, 170, 170, 170, 162, 170, 255, 235, 240,   0,   0, 128,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  10, 138, 170, 170, 168, 191, 255, 235, 240, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0, 168,  10, 170, 170, 170, 170, 175, 235, 250, 240,  10,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  10,  42, 170, 175, 255, 175, 235, 234, 248,   0,  10,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   2,   0,  10, 170, 170, 191, 255, 239, 255, 234, 188,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,  42,   0,  10, 191, 250, 254, 175, 235, 254, 170, 252,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  15, 255, 250, 254, 175, 235, 250, 170,  56,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,  32,   3, 250, 254, 254, 171, 235, 250, 170, 160,  34, 128,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   8,   3, 250, 250, 254, 171, 234, 254, 168, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   2,  11, 255, 250, 191, 175, 234, 254, 170, 130,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  10, 255, 170, 191, 255, 170, 250, 170,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  10, 254, 170, 175, 254, 170, 170,  42, 160,   0,   0,   0,   0, 160,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,  32,  10, 254, 170, 170, 170, 170, 168, 170, 162,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,  32,   2, 191, 170, 138, 170, 170, 170, 170, 128, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   2, 128,   2, 191,  42, 162, 170, 170, 162, 170,   0,   8,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2, 186, 138,  40, 170, 170, 168,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 162, 170, 168,  10, 170, 170, 170, 128,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,  10,   2, 170, 168,  10, 170, 170,  42,  42, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,  32,   2, 170, 168,   0,   0,   0, 168, 162, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 162,  40,  32,   0,   0, 170, 168, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  42, 168,   8,   0,   2, 168, 170, 128,  32,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2, 128,   2,   0,   2, 170, 170,   0,  10,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 128,   0,   0,   0,   0, 170, 168,   0,   0, 160,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 128,   0,   0,   0, 128,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,  15, 255,   0, 255, 192,  63,   0, 252,  63, 255,   8,   0, 255, 192,  63,   3, 243, 255, 240, 255, 252,  63,   0,   0,   0,   0, 
  0,   0,   0,   0,  63, 255, 192, 255, 192,  63,   0, 252,  63, 255,   0,   3, 255, 240,  63,   3, 243, 255, 240, 255, 255,  63,   0,   0,   0,   0, 
  0,   0,   0,   0, 255,  15, 195, 255, 240,  63, 195, 252,  63,   0,   0,  15, 243, 252,  15, 207, 195, 240,   0, 252,  63,  63,   0,   0,   0,   0, 
  0,   0,   0,   0, 252,   0,   3, 243, 240,  63, 195, 252,  63,   0,   0,  15, 192, 252,  15, 207, 195, 240,   0, 252,  63,  63,   0,   0,   0,   0, 
  0,   0,   0,   0, 252,  63, 195, 243, 240,  60, 195,  60,  63, 255,   0,  15, 192, 252,  15, 207, 195, 255, 240, 255, 255,  63,   0,   0,   0,   0, 
  0,   0,   0,   0, 252,  63, 195, 243, 240,  60, 255,  60,  63, 255,   0,  15, 192, 252,   3, 207,   3, 255, 240, 255, 240,  63,   0,   0,   0,   0, 
  0,   0,   0,   0, 252,  15, 195, 255, 240,  60, 255,  60,  63,   0,   0,  15, 192, 252,   3, 207,   3, 240,   0, 252, 252,   0,   0,   0,   0,   0, 
  0,   0,   0,   0, 255,  15, 207, 255, 252,  60,  60,  60,  63,   0,   0,  15, 243, 252,   3, 255,   3, 240,   0, 252,  63,  63,   0,   0,   0,   0, 
  0,   0,   0,   0,  63, 255, 207, 192, 252,  60,  60,  60,  63, 255,   0,   3, 255, 240,   0, 252,   3, 255, 240, 252,  63,  63,   0,   0,   0,   0, 
  0,   0,   0,   0,  15, 252,  15, 192, 252,  60,  60,  60,  63, 255,   0,   0, 255, 192,   0, 252,   3, 255, 240, 252,  15, 255,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 
};

void PantallaFin() {
    VGAX::copy((byte*)img_game_over_data);
    

    while(command != BOTON_PRESS){
      checkCommand();
      VGAX::delay(33);
    }
    command = 0;

}
