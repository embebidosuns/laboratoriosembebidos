/*
 LCD1602 Keypad Shield 1.0 Test Sketch - LiquidCrystal Library
 
 Este sketch demuestra el uso del LCD1602 Key Shield 1.0.
 Para ello se toman los pulsos de los botones mediante la entrada
 analógica AD0 y, mediante la librería LiquidCrystal de Arduino 1.0,
 se muestra en el display la tecla pulsada. La librería LiquidCrystal
 permite controlar cualquier display LCD compatible con el 
 controlador Hitachi HD44780.
 
 La configuración de la Librería se realiza en base al esquemático
 del shield.
 
 Este ejemplo está basado en un ejemplo provisto con la documentación del
 LCD1602 Keypad Shield, el cuál ha sido adaptado en base a los ejemplos 
 de la librería LiquidCrystal de Arduino.
 
 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 http://arduino.cc/en/Reference/LiquidCrystal
 
 Adaptación por Sebastián Escarza.
 */


#include "Arduino.h"
// include the library code:
#include "LiquidCrystal.h"

// these constants won't change.  But you can change the size of
// your LCD using them:
const int numRows = 2;
const int numCols = 16;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

//Key message
String msgs[5] = {" Right Key:  OK ", 
                    " Up Key:     OK ", 
                    " Down Key:   OK ", 
                    " Left Key:   OK ", 
                    " Select Key: OK "};
int adc_key_val[5] ={30, 150, 360, 535, 760 };
int NUM_KEYS = 5;
int adc_key_in;
int key=-1;
int oldkey=-1;

void setup() {	
	lcd.begin(numCols,numRows);
}


double analog_to_lux(int analog){
	double coef = 0;

	if(analog <=200){
		if(analog == 200)
		coef = 1.0f;//(200 - analog);//0.005f; //1.0f/200.0f
		else
		coef = (200.0f - analog) / 200.0f;
	}
	if(analog>200 && analog <=300){
		coef = 1.0f + 2.0f * (analog - 200.0f) / 300.0f;//1.02f;//(3.0f-1.0f)/(300.0f-200.0f);
	}
	if(analog>300 && analog <=393){
		coef = 3.0f + 3.0f * (analog - 300.0f) / 393.0f;//1.032f * 3;//(6.0f-3.0f)/(393.0f-300.0f);
	}
	if(analog>393 && analog <511){
		coef = 6.0f + 4.0f * (analog - 393.0f) / 511.0f;//1.0339f * 6;//(10.0f - 6.0f)/(511.0f-393.0f);
	}
	if(analog>511 && analog <=601){
		coef = 10.0f + 5.0f * (analog - 511.0f) / 601.0f;//1.055f * 10;//(15.0f - 10.0f)/(601.0f-511.0f);
	}
	if(analog>601 && analog <=682){
		coef = 15.0f + 20.0f * (analog - 601.0f) / 682.0f;//1.2469f * 15;//(35.0f - 15.0f)/(682.0f-601.0f);
	}
	if(analog>682 && analog <=786){
		coef = 35.0f + 45.0f * (analog - 682) / 786.0f;//1.4327f * 35;//(80.0f - 35.0f)/(786.0f-682.0f);
	}
	
	if(analog>786 && analog <=930){
		coef = 85.0f + 15.0f * (analog - 786.0f) / 930.0f;//1.138f * 80;//(100.0f - 85.0f)/(930.0f-786.0f);
	}
	if(analog>930){
		coef = 100.0f;
	}
	return coef;
}

void clear() {
	lcd.setCursor(0, 0);
	lcd.print("                 ");
	lcd.setCursor(0, 1);
	lcd.print("                 ");
}

void loop()
{
	clear();
    adc_key_in = analogRead(1);     // read the value from the sensor  
	//lcd.setCursor(0, 0);
	double volt = adc_key_in * 5.0f / 1023;
	//lcd.print("Volt: ");
	//lcd.print(volt);
	lcd.setCursor(0, 0);
	lcd.print("R1: ");
	lcd.print((((5-volt)*10)/volt));
	lcd.print("K");

	//lcd.print(adc_key_in);
	lcd.setCursor(0, 1);
	double lux = analog_to_lux(adc_key_in);//exp(adc_key_in / 195.0f);
	lcd.print("LUX: ");
	lcd.print(lux);
	delay(1000);
}


