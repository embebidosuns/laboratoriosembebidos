/*
 LCD1602 Keypad Shield 1.0 Test Sketch - LiquidCrystal Library
 
 Este sketch demuestra el uso del LCD1602 Key Shield 1.0.
 Para ello se toman los pulsos de los botones mediante la entrada
 analógica AD0 y, mediante la librería LiquidCrystal de Arduino 1.0,
 se muestra en el display la tecla pulsada. La librería LiquidCrystal
 permite controlar cualquier display LCD compatible con el 
 controlador Hitachi HD44780.
 
 La configuración de la Librería se realiza en base al esquemático
 del shield.
 
 Este ejemplo está basado en un ejemplo provisto con la documentación del
 LCD1602 Keypad Shield, el cuál ha sido adaptado en base a los ejemplos 
 de la librería LiquidCrystal de Arduino.
 
 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 http://arduino.cc/en/Reference/LiquidCrystal
 
 Adaptación por Sebastián Escarza.
 */


#include "Arduino.h"
// include the library code:
#include "LiquidCrystal.h"
#include "keyboard.h"
#include "adc.h"

// these constants won't change.  But you can change the size of
// your LCD using them:
const int numRows = 2;
const int numCols = 16;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

void clear() {
	lcd.setCursor(0, 0);
	lcd.print("                 ");
	lcd.setCursor(0, 1);
	lcd.print("                 ");
}

void display(String evento, String tecla)
{
	clear();
		
	lcd.setCursor(0, 0);
	lcd.print("Event:");
	lcd.print(evento);

	lcd.setCursor(0, 1);
	lcd.print("Key:");
	lcd.print(tecla);
}

bool keyPress = false;
long tiempo = 0;

void onKeyUp_Up(){
	display("OnKeyUp", "TECLA_UP");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_Up(){
	display("OnKeyDown", "TECLA_UP");
	keyPress = false;
	
}

void onKeyUp_Down(){
	display("OnKeyUp", "TECLA_DOWN");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_Down(){
	display("OnKeyDown", "TECLA_DOWN");
	keyPress = false;

}

void onKeyUp_Left(){
	display("OnKeyUp", "TECLA_LEFT");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_Left(){
	display("OnKeyDown", "TECLA_LEFT");
	keyPress = false;

}

void onKeyUp_Right(){
	display("OnKeyUp", "TECLA_RIGHT");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_Right(){
	display("OnKeyDown", "TECLA_RIGHT");
	keyPress = false;
}

void onKeyUp_Select(){
	display("OnKeyUp", "TECLA_SELECT");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_Select(){
	display("OnKeyDown", "TECLA_SELECT");
	keyPress = false;

}

void onKeyUp_A2() {
	display("OnKeyUp", "BOTON_A2");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_A2() {
	display("OnKeyDown", "BOTON_A2");
	keyPress = false;

}

void onKeyUp_A3() {
	display("OnKeyUp", "BOTON_A3");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_A3() {
	display("OnKeyDown", "BOTON_A3");
	keyPress = false;

}

void onKeyUp_A4() {
	display("OnKeyUp", "BOTON_A4");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_A4() {
	display("OnKeyDown", "BOTON_A4");
	keyPress = false;

}

void onKeyUp_A5() {
	display("OnKeyUp", "BOTON_A5");
	keyPress = true;
	tiempo = millis();
}

void onKeyDown_A5() {
	display("OnKeyDown", "BOTON_A5");
	keyPress = false;

}

int valor = 0;
void aux_func(int aux){
	valor = aux;
}

struct adc_cfg other = { 1, aux_func };
	
void setup() {
	lcd.begin(numCols,numRows);
		
	//adc_add_config(&other);
	keyboard_init();

	key_down_callback(onKeyDown_Up, TECLA_UP);
	key_up_callback(onKeyUp_Up, TECLA_UP);
	key_down_callback(onKeyDown_Down, TECLA_DOWN);
	key_up_callback(onKeyUp_Down, TECLA_DOWN);
	key_down_callback(onKeyDown_Left, TECLA_LEFT);
	key_up_callback(onKeyUp_Left, TECLA_LEFT);
	key_down_callback(onKeyDown_Right, TECLA_RIGHT);
	key_up_callback(onKeyUp_Right, TECLA_RIGHT);
	key_down_callback(onKeyDown_Select, TECLA_SELECT);
	key_up_callback(onKeyUp_Select, TECLA_SELECT);
	key_down_callback(onKeyDown_A2, BOTON_A2);
	key_up_callback(onKeyUp_A2, BOTON_A2);
	key_down_callback(onKeyDown_A3, BOTON_A3);
	key_up_callback(onKeyUp_A3, BOTON_A3);
	key_down_callback(onKeyDown_A4, BOTON_A4);
	key_up_callback(onKeyUp_A4, BOTON_A4);
	key_down_callback(onKeyDown_A5, BOTON_A5);
	key_up_callback(onKeyUp_A5, BOTON_A5);
	
	tiempo = millis();
	lcd.setCursor(0, 0);
	lcd.print("KeyBoard driver  ");
	lcd.setCursor(0, 1);
	lcd.print("Press any key     ");
}

void loop()
{    
	
	if(keyPress)
		if(millis() - tiempo > 3000){
			tiempo = millis();
			lcd.setCursor(0, 0);
			lcd.print("KeyBoard driver  ");
			lcd.setCursor(0, 1);
			lcd.print("Press any key    ");
			//lcd.print(valor);
			
		}
	/**/
	
	keyboard_loop();
	delay(50);
}


