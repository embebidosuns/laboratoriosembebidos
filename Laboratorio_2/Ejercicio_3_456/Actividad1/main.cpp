/*
 LCD1602 Keypad Shield 1.0 Test Sketch - LiquidCrystal Library
 
 Este sketch demuestra el uso del LCD1602 Key Shield 1.0.
 Para ello se toman los pulsos de los botones mediante la entrada
 analógica AD0 y, mediante la librería LiquidCrystal de Arduino 1.0,
 se muestra en el display la tecla pulsada. La librería LiquidCrystal
 permite controlar cualquier display LCD compatible con el 
 controlador Hitachi HD44780.
 
 La configuración de la Librería se realiza en base al esquemático
 del shield.
 
 Este ejemplo está basado en un ejemplo provisto con la documentación del
 LCD1602 Keypad Shield, el cuál ha sido adaptado en base a los ejemplos 
 de la librería LiquidCrystal de Arduino.
 
 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 http://arduino.cc/en/Reference/LiquidCrystal
 
 Adaptación por Sebastián Escarza.
 */


#include "Arduino.h"
// include the library code:
#include "LiquidCrystal.h"
#include "adc.h"
#include "analog_to_lux.h"
// these constants won't change.  But you can change the size of
// your LCD using them:
const int numRows = 2;
const int numCols = 16;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

//Key message
String msgs[5] = {"Right", 
                    "Up", 
                    "Down", 
                    "Left", 
                    "Select"};
					
int adc_key_val[5] ={30, 150, 360, 535, 760 };
int NUM_KEYS = 5;
int adc_key_in;
int key=-1;
int oldkey=-1;


void clear(int line) {
	lcd.setCursor(0, line);
	lcd.print("                 ");
}

void display(int analogRead)
{
	clear(1);
		
	//double volt = analogRead * 5.0f / 1023;
	//lcd.setCursor(0, 0);
	//lcd.print("R1: ");
	//lcd.print((((5-volt)*10000)/volt) / 1000);
	//lcd.print(analogRead);

	double lux = analog_to_lux(analogRead);//exp(adc_key_in / 195.0f);
	lcd.setCursor(0, 1);
	lcd.print("LUX: ");
	lcd.print(lux);
}

int get_key(int input)
{
	int k;
	for (k = 0; k < NUM_KEYS; k++)
	if (input < adc_key_val[k]) {
		//prev_lcd_key = new_lcd_key;
		return k;
	}

	if (k >= NUM_KEYS)
	return -1;     // No valid key pressed

	
}

void button(int analogRead){
	
	clear(0);
	lcd.setCursor(0, 0);
	lcd.print("A_Read: ");
	int key = get_key(analogRead);
	if(key!=-1){
		
		
		lcd.print(msgs[key]);
	}
	else{
		lcd.print("NONE");
	}
	
}
struct adc_cfg cfg = { 1, display };
struct adc_cfg cfg2 = { 0, button };
	

void setup() {
	lcd.begin(numCols,numRows);
	cli();
	// Configurar e inicializar Driver ADC
	// Input Channel -> A1
	// Result Callback -> display(int analogRead)
	//aux_init(button);
	adc_add_config(&cfg2);

	adc_init(&cfg);
	sei();
}

void loop()
{    
	//adc_key_in = analogRead(1);     // read the value from the sensor  
	//display(adc_key_in)
	//delay(1000);
	
	// Loop Driver ADC
	
	adc_loop();
	
	delay (500);
}


