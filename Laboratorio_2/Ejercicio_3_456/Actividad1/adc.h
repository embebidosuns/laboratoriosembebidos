#ifndef adc_h
#define adc_h

  struct adc_cfg 
  {
	  // ADC Channel A0:A5
	  int channel;
	  void (*callback)(int);
  };


	int adc_add_config(struct adc_cfg *cfg);
	int adc_init(struct adc_cfg *cfg);
	void adc_loop();
	
	int set_channel(int channel);



	

#endif

