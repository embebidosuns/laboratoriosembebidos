#include "analog_to_lux.h"
#include "adc.h"


double analog_to_lux(int analog){
	double coef = 0;

	if(analog <=200){
		if(analog == 200)
		coef = 1.0f;//(200 - analog);//0.005f; //1.0f/200.0f
		else
		coef = (200.0f - analog) / 200.0f;
	}
	if(analog>200 && analog <=300){
		coef = 1.0f + 2.0f * (analog - 200.0f) / 300.0f;//1.02f;//(3.0f-1.0f)/(300.0f-200.0f);
	}
	if(analog>300 && analog <=393){
		coef = 3.0f + 3.0f * (analog - 300.0f) / 393.0f;//1.032f * 3;//(6.0f-3.0f)/(393.0f-300.0f);
	}
	if(analog>393 && analog <511){
		coef = 6.0f + 4.0f * (analog - 393.0f) / 511.0f;//1.0339f * 6;//(10.0f - 6.0f)/(511.0f-393.0f);
	}
	if(analog>511 && analog <=601){
		coef = 10.0f + 5.0f * (analog - 511.0f) / 601.0f;//1.055f * 10;//(15.0f - 10.0f)/(601.0f-511.0f);
	}
	if(analog>601 && analog <=682){
		coef = 15.0f + 20.0f * (analog - 601.0f) / 682.0f;//1.2469f * 15;//(35.0f - 15.0f)/(682.0f-601.0f);
	}
	if(analog>682 && analog <=786){
		coef = 35.0f + 45.0f * (analog - 682) / 786.0f;//1.4327f * 35;//(80.0f - 35.0f)/(786.0f-682.0f);
	}
	
	if(analog>786 && analog <=930){
		coef = 85.0f + 15.0f * (analog - 786.0f) / 930.0f;//1.138f * 80;//(100.0f - 85.0f)/(930.0f-786.0f);
	}
	if(analog>930){
		coef = 100.0f + 100.0f*(1023.0f - analog)/1023.0f;
	}
	return coef;
}


