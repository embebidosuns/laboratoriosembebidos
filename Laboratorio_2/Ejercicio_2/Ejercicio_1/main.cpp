/*
 LCD1602 Keypad Shield 1.0 Test Sketch - LiquidCrystal Library
 
 Este sketch demuestra el uso del LCD1602 Key Shield 1.0.
 Para ello se toman los pulsos de los botones mediante la entrada
 analógica AD0 y, mediante la librería LiquidCrystal de Arduino 1.0,
 se muestra en el display la tecla pulsada. La librería LiquidCrystal
 permite controlar cualquier display LCD compatible con el 
 controlador Hitachi HD44780.
 
 La configuración de la Librería se realiza en base al esquemático
 del shield.
 
 Este ejemplo está basado en un ejemplo provisto con la documentación del
 LCD1602 Keypad Shield, el cuál ha sido adaptado en base a los ejemplos 
 de la librería LiquidCrystal de Arduino.
 
 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 http://arduino.cc/en/Reference/LiquidCrystal
 
 Adaptación por Sebastián Escarza.
 */

#include "Arduino.h"
// include the library code:
#include "LiquidCrystal.h"

// these constants won't change.  But you can change the size of
// your LCD using them:
const int numRows = 2;
const int numCols = 16;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);


void setup() {
	
	pinMode(10, OUTPUT);
	// set up the LCD's number of columns and rows: 
	lcd.begin(numCols,numRows);

	analogWrite(10, 100); //Controla intensidad backlight
	lcd.setCursor(0, 0);
	lcd.print("Cronometro:");
	lcd.setCursor(0, 1);
	lcd.print("           ");
	delay(2000);
  
	DDRC &= ~(1 << DDC2);     // Clear the PC2 pin
	// PC2 (PCINT10 pin) is now an input

	PORTC |= (1 << PORTC2);    // turn On the Pull-up
	// PC2 is now an input with pull-up enabled

	PCICR |= (1 << PCIE1);    // set PCIE1 to enable PCMSK1 scan
	PCMSK1 |= (1 << PCINT10);  // set PCINT10 to trigger an interrupt on state change



	cli();      //Esta instrucción deshabilita a las interrupciones globales sin modificar el resto de las interrupciones

	TCCR2A = 0; // set entire TCCR0A register to 0
	TCCR2A = 0; // same for TCCR0B
	TCNT2  = 0; // initialize counter value to 0
	// set compare match register for 100.16025641025641 Hz increments
	OCR2A = 155; // = 16000000 / (1024 * 100) - 1 (must be <256)
	// turn on CTC mode
	TCCR2A |= (1 << WGM01);
	// Set CS02, CS01 and CS00 bits for 1024 prescaler
	TCCR2B = 0b0111;

	// enable timer compare interrupt
	TIMSK2 |= (1 << OCIE0A);
	
	sei();                    // turn on interrupts
	
}


double pi = 3.141592654 * 2.0f;
double tiempo = 0;

int get_min(unsigned long tiempo) {
	return tiempo / 6000;
}

int get_sec(unsigned long tiempo) {
	return (tiempo % 6000) / 100;
}

int get_cen(unsigned long tiempo) {
	return (tiempo % 100);
}

void print_time(unsigned long tiempo) {
	lcd.setCursor(0, 1);
	lcd.print("M:");
	if (get_min(tiempo) <= 9)
	lcd.print(0);
	lcd.print(get_min((tiempo)));
	lcd.print(" S:");
	if (get_sec(tiempo) <= 9)
	lcd.print(0);
	lcd.print(get_sec((tiempo)));
	lcd.print(" C:");
	if (get_cen(tiempo) <= 9)
	lcd.print(0);
	lcd.print(get_cen((tiempo)));
}

volatile int state = 0;
volatile int LastState = 2;

volatile bool pinchange = false;

volatile unsigned long tiempoContador = 0;

void loop()
{
/*
   {
		tiempo += 0.0003141592654;
		if(tiempo>=pi)
			tiempo = 0;

		analogWrite(10,(sin(tiempo)+1) * 127.5f);
   }
   /**/
   if(state == 1 && state !=LastState){
	   LastState = state;
	   lcd.setCursor(0, 0);
	   lcd.print("Cronometro:Start ");
   }
   
   
   if(state == 1){
	   //aumentamos en 1 el contador y redibujamos
	   print_time(tiempoContador);

   }
   
   
   if(state != LastState && state % 2 == 0){
	   if(state == 0){
		   print_time(0);
		   tiempoContador = 0;
	   }
	   else{
		   lcd.setCursor(0, 0);
		   lcd.print("Cronometro:Stop ");
	   }
	   LastState = state;
   }
   
}
volatile bool debounce = true;
volatile int lastState = 0;
volatile int newState;
ISR (PCINT1_vect)
{
	newState = (PINC & (1 << PINC2));
	//hacer el debounce
	if(newState!=lastState){
		//cambio de bajo a alto
		delay(50);
		newState = (PINC & (1 << PINC2));

		if(newState!=lastState){
			lastState = newState;
			if(newState){
				if(!pinchange){
					
					pinchange = true;
					
					state = (state + 1) % 3;
				}
			}
			else{
				//cambio de alto a bajo
				pinchange = false;
			}
		}
		
	}
	

}

ISR(TIMER2_COMPA_vect){
	if(state == 1)
		tiempoContador++;	
	
}