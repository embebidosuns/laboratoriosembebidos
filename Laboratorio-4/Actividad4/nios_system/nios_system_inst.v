	nios_system u0 (
		.clk_clk         (<connected-to-clk_clk>),         //      clk.clk
		.input0_export   (<connected-to-input0_export>),   //   input0.export
		.input1_export   (<connected-to-input1_export>),   //   input1.export
		.input2_export   (<connected-to-input2_export>),   //   input2.export
		.input3_export   (<connected-to-input3_export>),   //   input3.export
		.input4_export   (<connected-to-input4_export>),   //   input4.export
		.input5_export   (<connected-to-input5_export>),   //   input5.export
		.input6_export   (<connected-to-input6_export>),   //   input6.export
		.input7_export   (<connected-to-input7_export>),   //   input7.export
		.leds_export     (<connected-to-leds_export>),     //     leds.export
		.output0_export  (<connected-to-output0_export>),  //  output0.export
		.output1_export  (<connected-to-output1_export>),  //  output1.export
		.output2_export  (<connected-to-output2_export>),  //  output2.export
		.output3_export  (<connected-to-output3_export>),  //  output3.export
		.output4_export  (<connected-to-output4_export>),  //  output4.export
		.output5_export  (<connected-to-output5_export>),  //  output5.export
		.output6_export  (<connected-to-output6_export>),  //  output6.export
		.output7_export  (<connected-to-output7_export>),  //  output7.export
		.reset_reset_n   (<connected-to-reset_reset_n>),   //    reset.reset_n
		.switches_export (<connected-to-switches_export>)  // switches.export
	);

