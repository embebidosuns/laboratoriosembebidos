
module nios_system (
	clk_clk,
	input0_export,
	input1_export,
	input2_export,
	input3_export,
	input4_export,
	input5_export,
	input6_export,
	input7_export,
	leds_export,
	output0_export,
	output1_export,
	output2_export,
	output3_export,
	output4_export,
	output5_export,
	output6_export,
	output7_export,
	reset_reset_n,
	switches_export);	

	input		clk_clk;
	input	[7:0]	input0_export;
	input	[7:0]	input1_export;
	input	[7:0]	input2_export;
	input	[7:0]	input3_export;
	input	[7:0]	input4_export;
	input	[7:0]	input5_export;
	input	[7:0]	input6_export;
	input	[7:0]	input7_export;
	output	[7:0]	leds_export;
	output	[7:0]	output0_export;
	output	[7:0]	output1_export;
	output	[7:0]	output2_export;
	output	[7:0]	output3_export;
	output	[7:0]	output4_export;
	output	[7:0]	output5_export;
	output	[7:0]	output6_export;
	output	[7:0]	output7_export;
	input		reset_reset_n;
	input	[7:0]	switches_export;
endmodule
