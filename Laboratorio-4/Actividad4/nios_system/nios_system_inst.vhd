	component nios_system is
		port (
			clk_clk         : in  std_logic                    := 'X';             -- clk
			input0_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			input1_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			input2_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			input3_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			input4_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			input5_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			input6_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			input7_export   : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			leds_export     : out std_logic_vector(7 downto 0);                    -- export
			output0_export  : out std_logic_vector(7 downto 0);                    -- export
			output1_export  : out std_logic_vector(7 downto 0);                    -- export
			output2_export  : out std_logic_vector(7 downto 0);                    -- export
			output3_export  : out std_logic_vector(7 downto 0);                    -- export
			output4_export  : out std_logic_vector(7 downto 0);                    -- export
			output5_export  : out std_logic_vector(7 downto 0);                    -- export
			output6_export  : out std_logic_vector(7 downto 0);                    -- export
			output7_export  : out std_logic_vector(7 downto 0);                    -- export
			reset_reset_n   : in  std_logic                    := 'X';             -- reset_n
			switches_export : in  std_logic_vector(7 downto 0) := (others => 'X')  -- export
		);
	end component nios_system;

	u0 : component nios_system
		port map (
			clk_clk         => CONNECTED_TO_clk_clk,         --      clk.clk
			input0_export   => CONNECTED_TO_input0_export,   --   input0.export
			input1_export   => CONNECTED_TO_input1_export,   --   input1.export
			input2_export   => CONNECTED_TO_input2_export,   --   input2.export
			input3_export   => CONNECTED_TO_input3_export,   --   input3.export
			input4_export   => CONNECTED_TO_input4_export,   --   input4.export
			input5_export   => CONNECTED_TO_input5_export,   --   input5.export
			input6_export   => CONNECTED_TO_input6_export,   --   input6.export
			input7_export   => CONNECTED_TO_input7_export,   --   input7.export
			leds_export     => CONNECTED_TO_leds_export,     --     leds.export
			output0_export  => CONNECTED_TO_output0_export,  --  output0.export
			output1_export  => CONNECTED_TO_output1_export,  --  output1.export
			output2_export  => CONNECTED_TO_output2_export,  --  output2.export
			output3_export  => CONNECTED_TO_output3_export,  --  output3.export
			output4_export  => CONNECTED_TO_output4_export,  --  output4.export
			output5_export  => CONNECTED_TO_output5_export,  --  output5.export
			output6_export  => CONNECTED_TO_output6_export,  --  output6.export
			output7_export  => CONNECTED_TO_output7_export,  --  output7.export
			reset_reset_n   => CONNECTED_TO_reset_reset_n,   --    reset.reset_n
			switches_export => CONNECTED_TO_switches_export  -- switches.export
		);

