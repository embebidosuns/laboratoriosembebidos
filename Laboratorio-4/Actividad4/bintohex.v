
module bintohex(
	//deb,
	in,
	hex0,
	hex1,
	hex2
);





//=======================================================
//  Structural coding
//=======================================================
	input			[7:0]		in;
	
	output			[6:0]		hex0;
	output			[6:0]		hex1;
	output			[6:0]		hex2;
	
	
	wire P0;
	wire P1;
	wire P2;
	wire P3;
	wire P4;
	wire P5;
	wire P6;
	wire P7;
	wire P8;
	wire P9;
	
	bintobcd BCD1(
		//.deb(deb),
		.B0(in[0]),
		.B1(in[1]),
		.B2(in[2]),
		.B3(in[3]),
		.B4(in[4]),
		.B5(in[5]),
		.B6(in[6]),
		.B7(in[7]),
		
		.P0(P0),
		.P1(P1),
		.P2(P2),
		.P3(P3),
		.P4(P4),
		.P5(P5),
		.P6(P6),
		.P7(P7),
		.P8(P8),
		.P9(P9)
	
	);
	
	
	
	bcdtohex bcdhex0(
		.D(P0),
		.C(P1),
		.B(P2),
		.A(P3),
		.S(hex0)
	
	);
	
	bcdtohex bcdhex1(
		.D(P4),
		.C(P5),
		.B(P6),
		.A(P7),
		.S(hex1)
	
	);
	bcdtohex bcdhex2(
		.D(P8),
		.C(P9),
		.B(0),
		.A(0),
		.S(hex2)
	
	);
	
endmodule