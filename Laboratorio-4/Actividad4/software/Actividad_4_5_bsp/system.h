/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'nios2_qsys_0' in SOPC Builder design 'nios_system'
 * SOPC Builder design path: ../../nios_system.sopcinfo
 *
 * Generated: Mon Nov 12 17:20:39 ART 2018
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_qsys"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x00020820
#define ALT_CPU_CPU_FREQ 50000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0x12
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00000020
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 50000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x12
#define ALT_CPU_NAME "nios2_qsys_0"
#define ALT_CPU_RESET_ADDR 0x00000000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x00020820
#define NIOS2_CPU_FREQ 50000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0x12
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00000020
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x12
#define NIOS2_RESET_ADDR 0x00000000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_NIOS2_QSYS


/*
 * LEDs configuration
 *
 */

#define ALT_MODULE_CLASS_LEDs altera_avalon_pio
#define LEDS_BASE 0x21100
#define LEDS_BIT_CLEARING_EDGE_REGISTER 0
#define LEDS_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LEDS_CAPTURE 0
#define LEDS_DATA_WIDTH 8
#define LEDS_DO_TEST_BENCH_WIRING 0
#define LEDS_DRIVEN_SIM_VALUE 0
#define LEDS_EDGE_TYPE "NONE"
#define LEDS_FREQ 50000000
#define LEDS_HAS_IN 0
#define LEDS_HAS_OUT 1
#define LEDS_HAS_TRI 0
#define LEDS_IRQ -1
#define LEDS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LEDS_IRQ_TYPE "NONE"
#define LEDS_NAME "/dev/LEDs"
#define LEDS_RESET_VALUE 0
#define LEDS_SPAN 16
#define LEDS_TYPE "altera_avalon_pio"


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Cyclone V"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag_uart_0"
#define ALT_STDERR_BASE 0x21148
#define ALT_STDERR_DEV jtag_uart_0
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart_0"
#define ALT_STDIN_BASE 0x21148
#define ALT_STDIN_DEV jtag_uart_0
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart_0"
#define ALT_STDOUT_BASE 0x21148
#define ALT_STDOUT_DEV jtag_uart_0
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "nios_system"


/*
 * c_input_0 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_0 altera_avalon_pio
#define C_INPUT_0_BASE 0x21130
#define C_INPUT_0_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_0_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_0_CAPTURE 0
#define C_INPUT_0_DATA_WIDTH 8
#define C_INPUT_0_DO_TEST_BENCH_WIRING 0
#define C_INPUT_0_DRIVEN_SIM_VALUE 0
#define C_INPUT_0_EDGE_TYPE "NONE"
#define C_INPUT_0_FREQ 50000000
#define C_INPUT_0_HAS_IN 1
#define C_INPUT_0_HAS_OUT 0
#define C_INPUT_0_HAS_TRI 0
#define C_INPUT_0_IRQ -1
#define C_INPUT_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_0_IRQ_TYPE "NONE"
#define C_INPUT_0_NAME "/dev/c_input_0"
#define C_INPUT_0_RESET_VALUE 0
#define C_INPUT_0_SPAN 16
#define C_INPUT_0_TYPE "altera_avalon_pio"


/*
 * c_input_1 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_1 altera_avalon_pio
#define C_INPUT_1_BASE 0x210f0
#define C_INPUT_1_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_1_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_1_CAPTURE 0
#define C_INPUT_1_DATA_WIDTH 8
#define C_INPUT_1_DO_TEST_BENCH_WIRING 0
#define C_INPUT_1_DRIVEN_SIM_VALUE 0
#define C_INPUT_1_EDGE_TYPE "NONE"
#define C_INPUT_1_FREQ 50000000
#define C_INPUT_1_HAS_IN 1
#define C_INPUT_1_HAS_OUT 0
#define C_INPUT_1_HAS_TRI 0
#define C_INPUT_1_IRQ -1
#define C_INPUT_1_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_1_IRQ_TYPE "NONE"
#define C_INPUT_1_NAME "/dev/c_input_1"
#define C_INPUT_1_RESET_VALUE 0
#define C_INPUT_1_SPAN 16
#define C_INPUT_1_TYPE "altera_avalon_pio"


/*
 * c_input_2 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_2 altera_avalon_pio
#define C_INPUT_2_BASE 0x210d0
#define C_INPUT_2_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_2_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_2_CAPTURE 0
#define C_INPUT_2_DATA_WIDTH 8
#define C_INPUT_2_DO_TEST_BENCH_WIRING 0
#define C_INPUT_2_DRIVEN_SIM_VALUE 0
#define C_INPUT_2_EDGE_TYPE "NONE"
#define C_INPUT_2_FREQ 50000000
#define C_INPUT_2_HAS_IN 1
#define C_INPUT_2_HAS_OUT 0
#define C_INPUT_2_HAS_TRI 0
#define C_INPUT_2_IRQ -1
#define C_INPUT_2_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_2_IRQ_TYPE "NONE"
#define C_INPUT_2_NAME "/dev/c_input_2"
#define C_INPUT_2_RESET_VALUE 0
#define C_INPUT_2_SPAN 16
#define C_INPUT_2_TYPE "altera_avalon_pio"


/*
 * c_input_3 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_3 altera_avalon_pio
#define C_INPUT_3_BASE 0x210b0
#define C_INPUT_3_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_3_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_3_CAPTURE 0
#define C_INPUT_3_DATA_WIDTH 8
#define C_INPUT_3_DO_TEST_BENCH_WIRING 0
#define C_INPUT_3_DRIVEN_SIM_VALUE 0
#define C_INPUT_3_EDGE_TYPE "NONE"
#define C_INPUT_3_FREQ 50000000
#define C_INPUT_3_HAS_IN 1
#define C_INPUT_3_HAS_OUT 0
#define C_INPUT_3_HAS_TRI 0
#define C_INPUT_3_IRQ -1
#define C_INPUT_3_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_3_IRQ_TYPE "NONE"
#define C_INPUT_3_NAME "/dev/c_input_3"
#define C_INPUT_3_RESET_VALUE 0
#define C_INPUT_3_SPAN 16
#define C_INPUT_3_TYPE "altera_avalon_pio"


/*
 * c_input_4 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_4 altera_avalon_pio
#define C_INPUT_4_BASE 0x21090
#define C_INPUT_4_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_4_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_4_CAPTURE 0
#define C_INPUT_4_DATA_WIDTH 8
#define C_INPUT_4_DO_TEST_BENCH_WIRING 0
#define C_INPUT_4_DRIVEN_SIM_VALUE 0
#define C_INPUT_4_EDGE_TYPE "NONE"
#define C_INPUT_4_FREQ 50000000
#define C_INPUT_4_HAS_IN 1
#define C_INPUT_4_HAS_OUT 0
#define C_INPUT_4_HAS_TRI 0
#define C_INPUT_4_IRQ -1
#define C_INPUT_4_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_4_IRQ_TYPE "NONE"
#define C_INPUT_4_NAME "/dev/c_input_4"
#define C_INPUT_4_RESET_VALUE 0
#define C_INPUT_4_SPAN 16
#define C_INPUT_4_TYPE "altera_avalon_pio"


/*
 * c_input_5 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_5 altera_avalon_pio
#define C_INPUT_5_BASE 0x21070
#define C_INPUT_5_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_5_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_5_CAPTURE 0
#define C_INPUT_5_DATA_WIDTH 8
#define C_INPUT_5_DO_TEST_BENCH_WIRING 0
#define C_INPUT_5_DRIVEN_SIM_VALUE 0
#define C_INPUT_5_EDGE_TYPE "NONE"
#define C_INPUT_5_FREQ 50000000
#define C_INPUT_5_HAS_IN 1
#define C_INPUT_5_HAS_OUT 0
#define C_INPUT_5_HAS_TRI 0
#define C_INPUT_5_IRQ -1
#define C_INPUT_5_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_5_IRQ_TYPE "NONE"
#define C_INPUT_5_NAME "/dev/c_input_5"
#define C_INPUT_5_RESET_VALUE 0
#define C_INPUT_5_SPAN 16
#define C_INPUT_5_TYPE "altera_avalon_pio"


/*
 * c_input_6 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_6 altera_avalon_pio
#define C_INPUT_6_BASE 0x21050
#define C_INPUT_6_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_6_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_6_CAPTURE 0
#define C_INPUT_6_DATA_WIDTH 8
#define C_INPUT_6_DO_TEST_BENCH_WIRING 0
#define C_INPUT_6_DRIVEN_SIM_VALUE 0
#define C_INPUT_6_EDGE_TYPE "NONE"
#define C_INPUT_6_FREQ 50000000
#define C_INPUT_6_HAS_IN 1
#define C_INPUT_6_HAS_OUT 0
#define C_INPUT_6_HAS_TRI 0
#define C_INPUT_6_IRQ -1
#define C_INPUT_6_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_6_IRQ_TYPE "NONE"
#define C_INPUT_6_NAME "/dev/c_input_6"
#define C_INPUT_6_RESET_VALUE 0
#define C_INPUT_6_SPAN 16
#define C_INPUT_6_TYPE "altera_avalon_pio"


/*
 * c_input_7 configuration
 *
 */

#define ALT_MODULE_CLASS_c_input_7 altera_avalon_pio
#define C_INPUT_7_BASE 0x21030
#define C_INPUT_7_BIT_CLEARING_EDGE_REGISTER 0
#define C_INPUT_7_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_INPUT_7_CAPTURE 0
#define C_INPUT_7_DATA_WIDTH 8
#define C_INPUT_7_DO_TEST_BENCH_WIRING 0
#define C_INPUT_7_DRIVEN_SIM_VALUE 0
#define C_INPUT_7_EDGE_TYPE "NONE"
#define C_INPUT_7_FREQ 50000000
#define C_INPUT_7_HAS_IN 1
#define C_INPUT_7_HAS_OUT 0
#define C_INPUT_7_HAS_TRI 0
#define C_INPUT_7_IRQ -1
#define C_INPUT_7_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_INPUT_7_IRQ_TYPE "NONE"
#define C_INPUT_7_NAME "/dev/c_input_7"
#define C_INPUT_7_RESET_VALUE 0
#define C_INPUT_7_SPAN 16
#define C_INPUT_7_TYPE "altera_avalon_pio"


/*
 * c_output_0 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_0 altera_avalon_pio
#define C_OUTPUT_0_BASE 0x21120
#define C_OUTPUT_0_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_0_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_0_CAPTURE 0
#define C_OUTPUT_0_DATA_WIDTH 8
#define C_OUTPUT_0_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_0_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_0_EDGE_TYPE "NONE"
#define C_OUTPUT_0_FREQ 50000000
#define C_OUTPUT_0_HAS_IN 0
#define C_OUTPUT_0_HAS_OUT 1
#define C_OUTPUT_0_HAS_TRI 0
#define C_OUTPUT_0_IRQ -1
#define C_OUTPUT_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_0_IRQ_TYPE "NONE"
#define C_OUTPUT_0_NAME "/dev/c_output_0"
#define C_OUTPUT_0_RESET_VALUE 0
#define C_OUTPUT_0_SPAN 16
#define C_OUTPUT_0_TYPE "altera_avalon_pio"


/*
 * c_output_1 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_1 altera_avalon_pio
#define C_OUTPUT_1_BASE 0x210e0
#define C_OUTPUT_1_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_1_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_1_CAPTURE 0
#define C_OUTPUT_1_DATA_WIDTH 8
#define C_OUTPUT_1_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_1_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_1_EDGE_TYPE "NONE"
#define C_OUTPUT_1_FREQ 50000000
#define C_OUTPUT_1_HAS_IN 0
#define C_OUTPUT_1_HAS_OUT 1
#define C_OUTPUT_1_HAS_TRI 0
#define C_OUTPUT_1_IRQ -1
#define C_OUTPUT_1_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_1_IRQ_TYPE "NONE"
#define C_OUTPUT_1_NAME "/dev/c_output_1"
#define C_OUTPUT_1_RESET_VALUE 0
#define C_OUTPUT_1_SPAN 16
#define C_OUTPUT_1_TYPE "altera_avalon_pio"


/*
 * c_output_2 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_2 altera_avalon_pio
#define C_OUTPUT_2_BASE 0x210c0
#define C_OUTPUT_2_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_2_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_2_CAPTURE 0
#define C_OUTPUT_2_DATA_WIDTH 8
#define C_OUTPUT_2_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_2_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_2_EDGE_TYPE "NONE"
#define C_OUTPUT_2_FREQ 50000000
#define C_OUTPUT_2_HAS_IN 0
#define C_OUTPUT_2_HAS_OUT 1
#define C_OUTPUT_2_HAS_TRI 0
#define C_OUTPUT_2_IRQ -1
#define C_OUTPUT_2_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_2_IRQ_TYPE "NONE"
#define C_OUTPUT_2_NAME "/dev/c_output_2"
#define C_OUTPUT_2_RESET_VALUE 0
#define C_OUTPUT_2_SPAN 16
#define C_OUTPUT_2_TYPE "altera_avalon_pio"


/*
 * c_output_3 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_3 altera_avalon_pio
#define C_OUTPUT_3_BASE 0x210a0
#define C_OUTPUT_3_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_3_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_3_CAPTURE 0
#define C_OUTPUT_3_DATA_WIDTH 8
#define C_OUTPUT_3_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_3_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_3_EDGE_TYPE "NONE"
#define C_OUTPUT_3_FREQ 50000000
#define C_OUTPUT_3_HAS_IN 0
#define C_OUTPUT_3_HAS_OUT 1
#define C_OUTPUT_3_HAS_TRI 0
#define C_OUTPUT_3_IRQ -1
#define C_OUTPUT_3_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_3_IRQ_TYPE "NONE"
#define C_OUTPUT_3_NAME "/dev/c_output_3"
#define C_OUTPUT_3_RESET_VALUE 0
#define C_OUTPUT_3_SPAN 16
#define C_OUTPUT_3_TYPE "altera_avalon_pio"


/*
 * c_output_4 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_4 altera_avalon_pio
#define C_OUTPUT_4_BASE 0x21080
#define C_OUTPUT_4_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_4_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_4_CAPTURE 0
#define C_OUTPUT_4_DATA_WIDTH 8
#define C_OUTPUT_4_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_4_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_4_EDGE_TYPE "NONE"
#define C_OUTPUT_4_FREQ 50000000
#define C_OUTPUT_4_HAS_IN 0
#define C_OUTPUT_4_HAS_OUT 1
#define C_OUTPUT_4_HAS_TRI 0
#define C_OUTPUT_4_IRQ -1
#define C_OUTPUT_4_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_4_IRQ_TYPE "NONE"
#define C_OUTPUT_4_NAME "/dev/c_output_4"
#define C_OUTPUT_4_RESET_VALUE 0
#define C_OUTPUT_4_SPAN 16
#define C_OUTPUT_4_TYPE "altera_avalon_pio"


/*
 * c_output_5 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_5 altera_avalon_pio
#define C_OUTPUT_5_BASE 0x21060
#define C_OUTPUT_5_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_5_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_5_CAPTURE 0
#define C_OUTPUT_5_DATA_WIDTH 8
#define C_OUTPUT_5_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_5_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_5_EDGE_TYPE "NONE"
#define C_OUTPUT_5_FREQ 50000000
#define C_OUTPUT_5_HAS_IN 0
#define C_OUTPUT_5_HAS_OUT 1
#define C_OUTPUT_5_HAS_TRI 0
#define C_OUTPUT_5_IRQ -1
#define C_OUTPUT_5_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_5_IRQ_TYPE "NONE"
#define C_OUTPUT_5_NAME "/dev/c_output_5"
#define C_OUTPUT_5_RESET_VALUE 0
#define C_OUTPUT_5_SPAN 16
#define C_OUTPUT_5_TYPE "altera_avalon_pio"


/*
 * c_output_6 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_6 altera_avalon_pio
#define C_OUTPUT_6_BASE 0x21040
#define C_OUTPUT_6_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_6_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_6_CAPTURE 0
#define C_OUTPUT_6_DATA_WIDTH 8
#define C_OUTPUT_6_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_6_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_6_EDGE_TYPE "NONE"
#define C_OUTPUT_6_FREQ 50000000
#define C_OUTPUT_6_HAS_IN 0
#define C_OUTPUT_6_HAS_OUT 1
#define C_OUTPUT_6_HAS_TRI 0
#define C_OUTPUT_6_IRQ -1
#define C_OUTPUT_6_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_6_IRQ_TYPE "NONE"
#define C_OUTPUT_6_NAME "/dev/c_output_6"
#define C_OUTPUT_6_RESET_VALUE 0
#define C_OUTPUT_6_SPAN 16
#define C_OUTPUT_6_TYPE "altera_avalon_pio"


/*
 * c_output_7 configuration
 *
 */

#define ALT_MODULE_CLASS_c_output_7 altera_avalon_pio
#define C_OUTPUT_7_BASE 0x21020
#define C_OUTPUT_7_BIT_CLEARING_EDGE_REGISTER 0
#define C_OUTPUT_7_BIT_MODIFYING_OUTPUT_REGISTER 0
#define C_OUTPUT_7_CAPTURE 0
#define C_OUTPUT_7_DATA_WIDTH 8
#define C_OUTPUT_7_DO_TEST_BENCH_WIRING 0
#define C_OUTPUT_7_DRIVEN_SIM_VALUE 0
#define C_OUTPUT_7_EDGE_TYPE "NONE"
#define C_OUTPUT_7_FREQ 50000000
#define C_OUTPUT_7_HAS_IN 0
#define C_OUTPUT_7_HAS_OUT 1
#define C_OUTPUT_7_HAS_TRI 0
#define C_OUTPUT_7_IRQ -1
#define C_OUTPUT_7_IRQ_INTERRUPT_CONTROLLER_ID -1
#define C_OUTPUT_7_IRQ_TYPE "NONE"
#define C_OUTPUT_7_NAME "/dev/c_output_7"
#define C_OUTPUT_7_RESET_VALUE 0
#define C_OUTPUT_7_SPAN 16
#define C_OUTPUT_7_TYPE "altera_avalon_pio"


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 32
#define ALT_SYS_CLK TIMER_0
#define ALT_TIMESTAMP_CLK none


/*
 * jtag_uart_0 configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart_0 altera_avalon_jtag_uart
#define JTAG_UART_0_BASE 0x21148
#define JTAG_UART_0_IRQ 0
#define JTAG_UART_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_UART_0_NAME "/dev/jtag_uart_0"
#define JTAG_UART_0_READ_DEPTH 64
#define JTAG_UART_0_READ_THRESHOLD 8
#define JTAG_UART_0_SPAN 8
#define JTAG_UART_0_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_0_WRITE_DEPTH 64
#define JTAG_UART_0_WRITE_THRESHOLD 8


/*
 * onchip_memory2_0 configuration
 *
 */

#define ALT_MODULE_CLASS_onchip_memory2_0 altera_avalon_onchip_memory2
#define ONCHIP_MEMORY2_0_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define ONCHIP_MEMORY2_0_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define ONCHIP_MEMORY2_0_BASE 0x0
#define ONCHIP_MEMORY2_0_CONTENTS_INFO ""
#define ONCHIP_MEMORY2_0_DUAL_PORT 0
#define ONCHIP_MEMORY2_0_GUI_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_MEMORY2_0_INIT_CONTENTS_FILE "nios_system_onchip_memory2_0"
#define ONCHIP_MEMORY2_0_INIT_MEM_CONTENT 1
#define ONCHIP_MEMORY2_0_INSTANCE_ID "NONE"
#define ONCHIP_MEMORY2_0_IRQ -1
#define ONCHIP_MEMORY2_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ONCHIP_MEMORY2_0_NAME "/dev/onchip_memory2_0"
#define ONCHIP_MEMORY2_0_NON_DEFAULT_INIT_FILE_ENABLED 0
#define ONCHIP_MEMORY2_0_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_MEMORY2_0_READ_DURING_WRITE_MODE "DONT_CARE"
#define ONCHIP_MEMORY2_0_SINGLE_CLOCK_OP 0
#define ONCHIP_MEMORY2_0_SIZE_MULTIPLE 1
#define ONCHIP_MEMORY2_0_SIZE_VALUE 131072
#define ONCHIP_MEMORY2_0_SPAN 131072
#define ONCHIP_MEMORY2_0_TYPE "altera_avalon_onchip_memory2"
#define ONCHIP_MEMORY2_0_WRITABLE 1


/*
 * switches configuration
 *
 */

#define ALT_MODULE_CLASS_switches altera_avalon_pio
#define SWITCHES_BASE 0x21110
#define SWITCHES_BIT_CLEARING_EDGE_REGISTER 0
#define SWITCHES_BIT_MODIFYING_OUTPUT_REGISTER 0
#define SWITCHES_CAPTURE 0
#define SWITCHES_DATA_WIDTH 8
#define SWITCHES_DO_TEST_BENCH_WIRING 0
#define SWITCHES_DRIVEN_SIM_VALUE 0
#define SWITCHES_EDGE_TYPE "NONE"
#define SWITCHES_FREQ 50000000
#define SWITCHES_HAS_IN 1
#define SWITCHES_HAS_OUT 0
#define SWITCHES_HAS_TRI 0
#define SWITCHES_IRQ -1
#define SWITCHES_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SWITCHES_IRQ_TYPE "NONE"
#define SWITCHES_NAME "/dev/switches"
#define SWITCHES_RESET_VALUE 0
#define SWITCHES_SPAN 16
#define SWITCHES_TYPE "altera_avalon_pio"


/*
 * sysid_qsys_0 configuration
 *
 */

#define ALT_MODULE_CLASS_sysid_qsys_0 altera_avalon_sysid_qsys
#define SYSID_QSYS_0_BASE 0x21140
#define SYSID_QSYS_0_ID 0
#define SYSID_QSYS_0_IRQ -1
#define SYSID_QSYS_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_QSYS_0_NAME "/dev/sysid_qsys_0"
#define SYSID_QSYS_0_SPAN 8
#define SYSID_QSYS_0_TIMESTAMP 1542053092
#define SYSID_QSYS_0_TYPE "altera_avalon_sysid_qsys"


/*
 * timer_0 configuration
 *
 */

#define ALT_MODULE_CLASS_timer_0 altera_avalon_timer
#define TIMER_0_ALWAYS_RUN 0
#define TIMER_0_BASE 0x21000
#define TIMER_0_COUNTER_SIZE 32
#define TIMER_0_FIXED_PERIOD 0
#define TIMER_0_FREQ 50000000
#define TIMER_0_IRQ 1
#define TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define TIMER_0_LOAD_VALUE 49999999
#define TIMER_0_MULT 1.0
#define TIMER_0_NAME "/dev/timer_0"
#define TIMER_0_PERIOD 1
#define TIMER_0_PERIOD_UNITS "s"
#define TIMER_0_RESET_OUTPUT 0
#define TIMER_0_SNAPSHOT 1
#define TIMER_0_SPAN 32
#define TIMER_0_TICKS_PER_SEC 1
#define TIMER_0_TIMEOUT_PULSE_OUTPUT 0
#define TIMER_0_TYPE "altera_avalon_timer"

#endif /* __SYSTEM_H_ */
