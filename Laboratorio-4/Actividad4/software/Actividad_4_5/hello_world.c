/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <system.h>
#include <sys/alt_irq.h>
#include <altera_avalon_pio_regs.h>
#include <altera_avalon_timer_regs.h>

#define array_size	8

int array [array_size];
volatile int flag_timer = 0;
int index = 0;

int old_value;
int new_value;

static void timerISR(void* context)
{
    flag_timer = 1;
	//Limpia el flag de interrupci�n (evita la ejecuci�n m�ltiple de la ISR ante la misma IRQ)
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE, 0);
}

void leerC()
{
	array[0] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_0_BASE);
	array[1] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_1_BASE);
	array[2] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_2_BASE);
	array[3] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_3_BASE);
	array[4] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_4_BASE);
	array[5] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_5_BASE);
	array[6] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_6_BASE);
	array[7] = IORD_ALTERA_AVALON_PIO_DATA(C_OUTPUT_7_BASE);
	//printf("leerC\n");

}

void enviar2C()
{
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_0_BASE, array[0]);
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_1_BASE, array[1]);
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_2_BASE, array[2]);
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_3_BASE, array[3]);
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_4_BASE, array[4]);
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_5_BASE, array[5]);
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_6_BASE, array[6]);
	IOWR_ALTERA_AVALON_PIO_DATA(C_INPUT_7_BASE, array[7]);
	printf("enviar2C\n");

}

int main()
{
	//Inicializa el Timer escribiendo su registro de control.
	IOWR_ALTERA_AVALON_TIMER_CONTROL (TIMER_0_BASE,
              ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  |
              ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
              ALTERA_AVALON_TIMER_CONTROL_START_MSK);


	//Registra la ISR (callback) del Timer
	int regStatus = alt_ic_isr_register(TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID,
		  	  	  	  TIMER_0_IRQ,
		  	  	  	  &timerISR, 0x0, 0x0);

	int i;
	for (i = 0; i<array_size; i++)
		array[i] = 0;
	enviar2C();//Se envia el arreglo al modulo C
	leerC();

	old_value = IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE);
	new_value = old_value;

	//Ciclo principal vac�o a la espera de interrupciones
	int a;
	while (1)
	{
		if (flag_timer)
        {
			a = array[index]
            IOWR_ALTERA_AVALON_PIO_DATA(LEDS_BASE, array[index]);
           


			index = (index + 1) % array_size;
            flag_timer = 0;
        }

		new_value = IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE);
		if (new_value != old_value)
		{
			array[0] = new_value;
			enviar2C();//Se envia el arreglo al modulo C
			leerC();
			old_value = new_value;
		}
	}
	return 0;
}



