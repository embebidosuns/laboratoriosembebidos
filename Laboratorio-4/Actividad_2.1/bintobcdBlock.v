
module bintobcdBlock(
	
	D,
	C,
	B,
	A,
	S0,
	S1,
	S2,
	S3,
);





//=======================================================
//  Structural coding
//=======================================================
	input			A;
	input			B;
	input			C;
	input			D;
	output			S0;
	output			S1;
	output			S2;
	output			S3;
	
	assign S3 = A | B&D | C&B;
	 
	
	
	assign S2 = ~C&~D&B | A&D;
	
	
	assign S1 = C&~B | C&D | A&~D;
	 //B'C + CD + AD'
	assign S0 = ~A&~B&D | B&C&~D | ~D&A;
	//AD' + A'B'D + BCD'

	
	
	
endmodule