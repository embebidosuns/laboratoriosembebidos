
module bcdtohex(
	
	A,
	B,
	C,
	D,
	S
);





//=======================================================
//  Structural coding
//=======================================================
	input			A;
	input			B;
	input			C;
	input			D;
	output			[6:0]	S;
	//a = BC'D' | A'B'C'D
	assign S[0] = B&~C&~D | ~A&~B&~C&D;
	//b =  BC'D + BCD'
	assign S[1] = B&~C&D | B&C&~D;
	//c =  B'CD'
	assign S[2] = ~B&C&~D;
	//d = BC'D' | BCD | A'B'C'D
	assign S[3] =  B&~C&~D | B&C&D | ~A&~B&~C&D;
	//e =  D | BC'
	assign S[4] =  D | B&~C;
	//f = B'C | CD | A'B'D
	assign S[5] =  ~B&C | C&D | ~A&~B&D;
	//g = A'B'C' | BCD
	assign S[6] =  ~A&~B&~C | B&C&D;
	

	
	
	
endmodule