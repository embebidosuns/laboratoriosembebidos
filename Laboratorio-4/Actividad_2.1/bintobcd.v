
module bintobcd(
	
	
	B0,
	B1,
	B2,
	B3,
	B4,
	B5,
	B6,
	B7,
	
	P0,
	P1,
	P2,
	P3,
	P4,
	P5,
	P6,
	P7,
	P8,
	P9
);





//=======================================================
//  Structural coding
//=======================================================
	input			B0;
	input			B1;
	input			B2;
	input			B3;
	input			B4;
	input			B5;
	input			B6;
	input			B7;
	
	
	output			P0;
	output			P1;
	output			P2;
	output			P3;
	output			P4;
	output			P5;
	output			P6;
	output			P7;
	output			P8;
	output			P9;
	
	assign P0 = B0;
	
	wire		C1_C2_1;
	wire		C1_C2_2;
	wire		C1_C2_3;
	wire		C1_C6;
	
	wire		C2_C3_1;
	wire		C2_C3_2;
	wire		C2_C3_3;
	wire		C2_C6;
	
	wire		C3_C4_1;
	wire		C3_C4_2;
	wire		C3_C4_3;
	wire		C3_C6;
	
	wire		C4_C5_1;
	wire		C4_C5_2;
	wire		C4_C5_3;
	wire		C4_C7;
	
	
	wire		C6_C7_1;
	wire		C6_C7_2;
	wire		C6_C7_3;
	
	bintobcdBlock C1(
		.D(B5),
		.C(B6),
		.B(B7),
		.A(0),
		.S0(C1_C2_1),
		.S1(C1_C2_2),
		.S2(C1_C2_3),
		.S3(C1_C6),
	
	);
	
	
	
	bintobcdBlock C2(
		.D(B4),
		.C(C1_C2_1),
		.B(C1_C2_2),
		.A(C1_C2_3),
		.S0(C2_C3_1),
		.S1(C2_C3_2),
		.S2(C2_C3_3),
		.S3(C2_C6),
	
	);
	
	bintobcdBlock C3(
		.D(B3),
		.C(C2_C3_1),
		.B(C2_C3_2),
		.A(C2_C3_3),
		.S0(C3_C4_1),
		.S1(C3_C4_2),
		.S2(C3_C4_3),
		.S3(C3_C6),
	
	);
	
	bintobcdBlock C4(
		.D(B2),
		.C(C3_C4_1),
		.B(C3_C4_2),
		.A(C3_C4_3),
		.S0(C4_C5_1),
		.S1(C4_C5_2),
		.S2(C4_C5_3),
		.S3(C4_C7),
	
	);
	
	bintobcdBlock C5(
		.D(B1),
		.C(C4_C5_1),
		.B(C4_C5_2),
		.A(C4_C5_3),
		.S0(P1),
		.S1(P2),
		.S2(P3),
		.S3(P4),
	
	);
	
	bintobcdBlock C6(
		.D(C3_C6),
		.C(C2_C6),
		.B(C1_C6),
		.A(0),
		.S0(C6_C7_1),
		.S1(C6_C7_2),
		.S2(C6_C7_3),
		.S3(P9),
	
	);
	
	bintobcdBlock C7(
		.D(C4_C7),
		.C(C6_C7_1),
		.B(C6_C7_2),
		.A(C6_C7_3),
		.S0(P5),
		.S1(P6),
		.S2(P7),
		.S3(P8),
	
	);
	
	
endmodule